%esercizio 1
x = [5, 13, 9, 12, 7, 4, 8, 6, 6, 10, 7, 11, 10, 8, 15, 8, 6, 9, 12, 10, 7, 11, 10, 8, 12, 9, 7, 10, 7, 8];
[freq,val] = hist(x,unique(x))
subplot(2,2,1)
stem(val,freq);
subplot(2,2,2)
plot(val,freq);

sumFreq = sum(freq);
relativefreq = freq/sumFreq;

subplot(2,2,3)
stem(val,relativefreq);
subplot(2,2,4)
plot(val,relativefreq);