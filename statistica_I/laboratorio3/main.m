n = 1000;
E = [1,2];
res = randi([1,6],1,n);
F = [2,3,6];
T = [1,4,5];
nE = zeros(n,1);
nF = zeros(n,1);
nT = zeros(n,1);
nEF = zeros(n,1);
nFT = zeros(n,1);

for k = 1:n
  nE(k) = sum(E == res(k));
  nF(k) = sum(F == res(k));
  nT(k) = sum(T == res(k));
  
  nEF(k) = sum(E == res(k)) && sum(F == res(k));
  nFT(k) = sum(T == res(k)) && sum(F == res(k));
end

PE = cumsum(nE) ./ (1:n)';
PF = cumsum(nF) ./ (1:n)';
PEcF = cumsum(nEF) ./ cumsum(nF);
PTcF = cumsum(nFT) ./ cumsum(nT);
