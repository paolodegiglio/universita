n = 4;
d = 365;
n_Bday = factorial(n) * nchoosek(d,n);
Percentage = factorial(n).*nchoosek(d,n)./(d.^n)

k = 100;
for i=1:k
  Percentage(i) = factorial(i).*nchoosek(d,i)./(d.^i)
endfor

plot(Percentage);
