function runsum = runsum(n)
  sum = 0;
  for i = 0:n
    sum += i;
  endfor
endfunction