clear;
N = 10;
data = 20*rand(N,1);
media = mean(data);
mediana = median(data);
deviazioneSt = std(data);
varianza = var(data);
q1 = quantile(0.25,data);
q2 = quantile(0.5,data);
q3 = quantile(0.75,data);
%boxplot(data, 'orientation','horizzontal');
percentile40 = prctile(data,40);

clear;
N = 100;
weekdays = 7;
data = zeros(N,weekdays);
for i = 1:weekdays
  data(:,i) = 7 + 6*sin(i/5)+randn(N,1);
endfor
%figure(1);
%boxplot(data);
%set(gca(),'xtick',[1:weekdays],'xticklabel','MO','TU','WE','TH','FR','SA','SU');

clear;
y = [92,94,93,96,93,94,95,96,91,93,95,95,95,92,93,94,91,94,92,93];
%figure(1);
%hist(y);

clear;
n = 100;
mu_B = 21;
sigma_B = 3;
mu_C = 23;
sigma_C = 2;
B = ceil(normrnd(mu_B,sigma_B,1,n));
C = ceil(normrnd(mu_C,sigma_C,1,n));
students= [B C];
min = min(students);
max = max(students);
median = median(students);
mean = mean(students);
range = range(students);
var = var(students);
sk = skewness(students);
k = kurtosis(students);

quartile = quantile(students,[0.25,0.5,0.75]);
pAll = prctile(students, [1:100]);
plot(pAll);