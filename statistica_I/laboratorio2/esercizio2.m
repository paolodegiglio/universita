load icecream_example.dat;
Temp = icecream_example(:,2); 
Prod = icecream_example(:,3);
month = [1:12];

subplot(1,3,1);
plot(month, Temp);

subplot(1,3,2);
plot(month, Prod);

subplot(1,3,3);
scatter(Temp, Prod);

corrcoef(Temp, Prod);