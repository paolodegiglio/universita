#include <stdio.h>
#define N 5

/* 
Scrivere un sottoprogramma che riceve come parametro una matrice quadrata 5x5 di caratteri (qualsiasi valore char). 
Il sottoprogramma individua il carattere che compare più frequentemente e lo restituisce al chiamante (si ipotizzi 
che sia sempre unico). Inoltre il sottoprogramma visualizza il contenuto della matrice, mostrando però uno spazio 
al posto dei caratteri uguali al carattere individuato.

Esempio:

Data la matrice            Il sottoprogramma restituirà ‘f’

in ingresso:               e stamperà a video il seguente testo

a.cde                      a.cde

fdffr                       d  r

tQfrd           ->         tQ rd

yyfwe                      yy we

f1bpf                       1bp

*/

/**
 * Funzione usata per caricare la matrice
 * 
 * @matrix[N][N] matrice quadrata da caricare con dimensione N 
 */
void caricaMatrice(char matrix[N][N])
{
    int i, j;
    char input, temp;

    printf("Caricamento matrice di dimensione %d \n", N);
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            printf("Inserisci il valore in %d %d ", i, j);
            scanf("%c", &input);
            scanf("%c", &temp);
            matrix[i][j] = input;
        }
    }
}

/**
 * Cerco nell'array se si trova il carattere, se si trova aumento il rispettivo contatore nell'array 
 * di appoggio con i contatori altrimenti lo aggiungo alla fine con contatore a 1
 * 
 * @arrayTempCont[N * N] int: array con contatori
 * @arrayTempChar[N][N] char: array con caratteri registrati 
 * @inputVal char: carattere da inserire
 * @length int: lunghezza array temporanei dei valori inseriti
 * 
 * Restituisce la length che indica la posizione dell'ultimo valore nell'array temporaneo
 */
int searchAndUpdateInArray(int arrayTempCont[N * N], char arrayTempChar[N * N], char inputVal, int length)
{

    int i, j, addable = 1;

    // Cerco se esiste il valore carattere nell'array temporaneo utilizzo un flag per controllarlo
    for (i = 0; i < length && addable == 1; i++)
    {
        if (arrayTempChar[i] == inputVal)
        {
            addable = 0;
            arrayTempCont[i]++;
        }
    }
    if (addable == 1)
    {
        arrayTempChar[length] = inputVal;
        arrayTempCont[length] = 1;
        length++;
    }
    return length;
}

/**
 * Funzione usata per calcolare l'elemento con frequenza massima
 * 
 * @matrix[N][N] matrice su cui fare il calcolo 
 */
char maxFreqValore(char matrix[N][N])
{
    int arrayTempCont[N * N] = {0};
    char arrayTempChar[N * N];
    int i, j, length = 0;

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            printf("%c", matrix[i][j]);
            /// Cerco e aggiorno gli array temporanei
            length = searchAndUpdateInArray(arrayTempCont, arrayTempChar, matrix[i][j], length);
        }
        printf("\n");
    }

    /// Trovo il valore con frequenza massima e lo restituisco alla fine
    int max = arrayTempCont[0];
    char charMax = arrayTempChar[0];
    for (int x = 0; x < length; x++)
    {
        if (max < arrayTempCont[i])
        {
            max = arrayTempCont[i];
            charMax = arrayTempChar[i];
        }
    }
    return charMax;
}

/**
 * Stampa con sostituzione
 * 
 * @matrix[N][N] matrice da mostrare
 * @inputVal valore da sostituire con lo spazio 
 */
void printWithSubstition(char matrix[N][N], char inputVal)
{
    int i, j;

    printf("\n");
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            if (matrix[i][j] == inputVal)
            {
                printf(" ");
            }
            else
            {
                printf("%c", matrix[i][j]);
            }
        }

        printf("\n");
    }
}

int main()
{
    char matrix[N][N];
    char valMax;

    /// Caricamento matrice
    caricaMatrice(matrix);

    /// Valore con massima frequente
    valMax = maxFreqValore(matrix);

    /// Stampa con sostituzione
    printWithSubstition(matrix, valMax);
    return 0;
}