#include <stdio.h>

/* Scrivere un programma che calcoli il massimo comun
divisore e il minimo comune multiplo di due numeri
inseriti dall’utente. Entrambi i numeri inseriti devono
essere maggiori di zero.  */

void multipliers(){
    int a, b, min, max;
    
    do{
        printf("Inserisci a, deve essere maggiore di 0\n");
        scanf("%d",&a);
    } while(a <= 0);

    do{
        printf("Inserisci b, deve essere maggiore di 0\n");
        scanf("%d",&b);
    } while(b <= 0);

    int mcm = a*b;
    int Mcd = 1;

    min = (a<b)?a:b;
    max = (a<b)?b:a;

    // Massimo comune divisore
    for(int i=2; i <= min; i++){
        if(a%i == 0 && b%i == 0){
            Mcd = i;
        }
    }

    // Minimo comune multiplo
    for(int j=min; j <= a*b; j++){
        if((min*j)%max == 0){
            mcm = min*j;
            break;
        }
    }

    printf("minimo comune multiplo %d, massimo comune divisore %d", mcm, Mcd);
}

/* Scrivere un programma che realizzi il gioco
Mastermind. Il primo giocatore inserisce 4 caratteri
che corrispondono alle iniziali di 4 colori tra (r = rosso;
g = giallo; v = verde, b = blu, n = nero; si assuma che
gli utenti inseriscano sempre una lettera compresa in
questo elenco). Il secondo giocatore ha a disposizione
10 tentativi per indovinare la combinazione, inserendo
a sua volta quattro caratteri ad ogni tentativo. Dopo
ogni tentativo il programma visualizza il numero di
colori corretti in posizione corretta (non vengono
segnalati i colori corretti in posizione sbagliata).
Terminati i tentativi, se il secondo giocatore non è
riuscito a indovinare la sequenza, vengono visualizzati
i caratteri corretti.  */

void mastermind(){
    char a1, b1, c1, d1;
    char a2, b2, c2, d2;
    int won = 0, correctColor;

    do{
        printf("Inserisci a1, deve essere compresa tra r,g,v,b,n\n");
        scanf("%c",&a1);
    } while(a1 != 'r' && a1 != 'b' && a1 != 'g' && a1 != 'v' && a1 != 'n');

    do{
        printf("Inserisci b1, deve essere compresa tra r,g,v,b,n\n");
        scanf("%c",&b1);
    } while(b1 != 'r' && b1 != 'b' && b1 != 'g' && b1 != 'v' && b1 != 'n');

    do{
        printf("Inserisci c1, deve essere compresa tra r,g,v,b,n\n");
        scanf("%c",&c1);
    } while(c1 != 'r' && c1 != 'b' && c1 != 'g' && c1 != 'v' && c1 != 'n');

    do{
        printf("Inserisci d1, deve essere compresa tra r,g,v,b,n\n");
        scanf("%c",&d1);
    } while(d1 != 'r' && d1 != 'b' && d1 != 'g' && d1 != 'v' && d1 != 'n');

    int i;
    
    for(i=0; i<10 && won == 0; i++){

        do{
            printf("Inserisci a2, deve essere compresa tra r,g,v,b,n\n");
            scanf("%c",&a2);
        } while(a2 != 'r' && a2 != 'b' && a2 != 'g' && a2 != 'v' && a2 != 'n');

        do{
            printf("Inserisci b2, deve essere compresa tra r,g,v,b,n\n");
            scanf("%c",&b2);
        } while(b2 != 'r' && b2 != 'b' && b2 != 'g' && b2 != 'v' && b2 != 'n');

        do{
            printf("Inserisci c2, deve essere compresa tra r,g,v,b,n\n");
            scanf("%c",&c2);
        } while(c2 != 'r' && c2 != 'b' && c2 != 'g' && c2 != 'v' && c2 != 'n');

        do{
            printf("Inserisci d2, deve essere compresa tra r,g,v,b,n\n");
            scanf("%c",&d2);
        } while(d2 != 'r' && d2 != 'b' && d2 != 'g' && d2 != 'v' && d2 != 'n');

        correctColor = 0;

        if(a1 == a2){
            correctColor ++;
        }

        if(b1 == b2){
            correctColor ++;
        }

        if(c1 == c2){
            correctColor ++;
        }
        
        if(d1 == d2){
            correctColor ++;
        }

        if(correctColor == 4){
            won = 1;
        } else {
            printf("colori corretti %d\n", correctColor);
        }
    }

    if(won == 1){
        printf("Tentativi usati %d\n", i);
    } else {
        printf("Hai perso %c%c%c%c\n",a1, b1, c1, d1);
    }
}

/* Chiamiamo coppia di quadrati (CQ) una coppia
<a,b> di numeri interi che sono uno il quadrato
dell’altro. Esempio <9,3> oppure <-3,9> (infatti
9=3*3).
Si codifichi un programma C che legge da
tastiera una sequenza che termina con 99 (di
lunghezza a priori illimitata) di numeri interi e
stampa a video quante coppie di numeri
consecutivi all’interno della sequenza
rappresentano una CQ.
Ad esempio: 2 4 16 0 3 9 99 contiene 3 CQ: <2,4>
<4,16> e <3,9> */

void perfectSquare(){
    int oldNumber = 99;
    int newNumber, tempNum;
    
    do {

        printf("Inserisci n\n");
        scanf("%d",&newNumber);

        tempNum = (newNumber>=0)? newNumber:newNumber*-1;

        if((tempNum*tempNum == oldNumber || oldNumber*oldNumber==tempNum) && oldNumber != 99){
            printf("%d, %d \n",oldNumber, newNumber);
        }
    
        //printf("%d, %d \n",oldNumber, tempNum);     

        oldNumber = newNumber;   

    } while (newNumber != 99);
}

/* Scrivere un programma C che richiede all’utente
10 voti (compresi tra 18 e 30) e li memorizza
all’interno di un array, quindi stampa l’array, il
voto massimo, minimo e medio. */

void array1(){
    int votes[10];

    for(int i = 0; i < 10; i++){
        do{
            printf("Inserisci il voto tra 18 e 30\n");
            scanf("%d",&votes[i]);
        }
        while(votes[i] <= 18 || votes[i] >= 30);  
    }

    int min = votes[0], max = min, sum = 0;
    float media;

    for(int i = 0; i < 10; i++){
        printf("%d ",votes[i]); 
        if(votes[i] < min){
            min = votes[i];
        }   
        if(votes[i] > max){
            max = votes[i];
        } 
        sum += votes[i]; 
    }
    media = sum/10;
    printf("\n, %d %d %0.2f", min, max, media);        
}

/* Scrivere un programma che dato un array di
caratteri lungo 10 (con i valori letti da standard
input) lo stampa, lo stampa invertito, sostituisce
ogni vocale minuscola con un ’*’ */

void array2(){
    char chars[10];
    char temp;

    for(int i = 0; i < 10; i++){
        printf("Inserisci la lettera\n");
        scanf("%c",&chars[i]);
        scanf("%c",&temp);
    }

    for(int j = 9; j >= 0; j--){
        if(chars[j] == 'a' || chars[j] == 'e' || chars[j] == 'j' || chars[j] == 'o' || chars[j] == 'u'){
            printf("* ");
        } else {
            printf("%c ", chars[j]);
        }
    }
}

/* Scrivere un programma che dato un array di interi
A ed un intero x determina quante occorrenze di x
sono in A. */

void array3(){
    int nums[10], count = 0;

    for(int i = 0; i < 10; i++){
        printf("Numero\n");
        scanf("%d",&nums[i]); 
    }

    printf("Numero\n");
    scanf("%d",&x);

    for(int i = 0; i < 10; i++){
        if(nums[i] == x){
            count ++;
        }
    }

    printf("%d", count);

}

int main() {

    printf("SELEZIONA L'ESERCIZIO\n");
    printf("1 Massimo comunue divisore e minimo comune multiplo di un numero\n");
    printf("2 Mastermind\n");
    printf("3 Sequenza con quadrati perfetti\n");
    printf("4 Array1\n");
    printf("5 Array2\n");
    printf("6 Array3\n");

    int chose = 6;

    if (chose == 0) {
        do {
            printf("INSERISCI 1/2/3/4/5/6\n");
            scanf("%d", &chose);
        } while (chose > 6 || chose < 1);
    }

    if (chose == 1) {
        multipliers();
    } else if (chose == 2) {
        mastermind();
    } else if (chose == 3) {
        perfectSquare();
    } else if (chose == 4) {
        array1();
    } else if (chose == 5) {
        array2();
    } else if (chose == 6) {
        array3();
    }

    return 0;
};