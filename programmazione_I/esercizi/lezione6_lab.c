#include <stdio.h>

/* ESERCIZIO 1
Scrivere un programma C che memorizza in un
array i primi 15 numeri di Fibonacci e li stampa a
video.
I primi due numeri di Fibonacci, Fib(0) e Fib(1), sono definiti come:
Fib(0) = 1
Fib(1) = 1
Il numero di Fibonacci di un valore n >= 2 è:
Fib(n) = Fib(n-2) + Fib(n-1) */

void fibonacci(){
    int fib[15];

    fib[0] = 1;
    fib[1] = 1;

    for(int i = 2; i <= 14; i++){
        fib[i] = fib[i-2] + fib[i-1];
    }

    for(int i = 0; i <= 14; i++){
        printf("%d ", fib[i]);
    }
}

/* ESERCIZIO 2
Scrivere un programma C che memorizza in un
array di lunghezza 15 tutti i numeri primi minori
di 50 e successivamente li stampa a video.
Un numero x è primo se è diverso da 1 ed ha
come unici divisori 1 ed x.
NB 0 non è primo in quanto ogni numero
divide 0 */
void numeriPrimi(){
    int primi[15], cont = 0, isPrime;

    for(int x = 1; x < 50; x++){

        isPrime = 1;
        for(int j = 2; j < (x/2)+1 && isPrime == 1; j++){
            if(x%j == 0)
                isPrime = 0;
        }
        if(isPrime == 1){
            primi[cont] = x;
            cont ++;
        }

    }

    for(int i = 0; i < cont; i++){
        printf("%d ", primi[i]);
    }
}

/* ESERCIZIO 3
Scrivere un programma che dato un array di
interi (lungo 10) con i valori inseriti da tastiera
determina quanti massimi locali ci sono nell’array
stampando il risultato.
Dato l’array int A[10]; A[i] è detto massimo locale
se uno dei seguenti casi è verificato
1. i=0 e A[0]>A[1]
2. i=9 e A[8]<A[9]
3. 0<i<9 e A[i-1]<A[i]<A[i+1] */

void massimoLocale(){
    int A[10];
    
    for(int i = 0; i < 10; i++){
        printf("Inserisci il numero n %d ", i);
        scanf("%d", &A[i]);
    }
    
    if(A[0] > A[1]){
        printf("%d ", A[0]);
    }

    if(A[9] > A[8]){
        printf("%d ", A[9]);
    }

    for(int i = 1; i <= 8; i++){
        if(A[i-1]<A[i] && A[i]<A[i+1]){
            printf("%d ", A[i]);
        }
    }
}

/* ESERCIZIO 4
Scrivere un programma C che:
○ Richiede all’utente di inserire 20 numeri interi a piacere e li
salva in un array.
○ Crea e stampa un array risultato in cui:
● Ogni elemento in posizione pari (n) è uguale alla somma
degli elementi dell’array originale in posizione pari
dall’inizio fino a n.
● Ogni elemento in posizione dispari (m) è uguale alla
somma degli elementi dell’array originale in posizione
dispari dall’inizio fino a m. */

void pariDispari(){
    int A[20], result[20], sum = 0;
    
    for(int i = 0; i < 20; i++){
        printf("Inserisci il numero n %d ", i);
        scanf("%d", &A[i]);
    }

    for(int i = 0; i < 20; i++){
        sum = 0;
        for(int j = i%2; j <= i; j+=2){
            sum+=A[j];
        }
        result[i] = sum; 
        printf("%d ", sum);
    }


}

/* ESERCIZIO 5
Scrivere un programma C che:
○ Richiede all’utente di inserire 20 caratteri e li salva in un
array.
○ Produce due risultati:
● Una array costruito con i caratteri inseriti in cui ogni
vocale (mauiscola o minuscola) è sostituita da un numero
corrispondente (A=1, E=2, I=3, O=4, U=5).
● Un numero contenente la somma delle cifre (caratteri da
0 a 9) contenute nella stringa prodotta. */

void sostituteString1(){
    
    char characters[20], temp2;
    int temp;
    
    for(int i = 0; i < 20; i++){
        printf("Inserisci il carattere n %d \n", i);
        scanf("%c", &characters[i]);
        scanf("%c",&temp2);
    }
    int sum = 0;
    for(int i = 0; i < 20; i++){
        if(characters[i] >= '0' && characters[i] <= '9'){
            temp = (int) characters[i];
            sum += temp - 48;
        }
        if(characters[i] == 'A' || characters[i] == 'a'){
            characters[i] = '1';
            sum += 1;
        }
        if(characters[i] == 'E' || characters[i] == 'i'){
            characters[i] = '2';
            sum += 2;
        }
        if(characters[i] == 'I' || characters[i] == 'i'){
            characters[i] = '3';
            sum += 3;
        }
        if(characters[i] == 'O' || characters[i] == 'o'){
            characters[i] = '4';
            sum += 4;
        }
        if(characters[i] == 'U' || characters[i] == 'u'){
            characters[i] = '5';
            sum += 5;
        }
        printf("%c ", characters[i]);

    }
    printf("\n Somma %d", sum);
}

/* ESERCIZIO 6
Scrivere un programma C che:
○ Richiede all’utente di inserire 20 caratteri che
memorizza in un array, e altri due caratteri c e r.
○ Restituisce una array costruito a partire dall’array
iniziale in cui ogni occorrenza di c è sostituita con r. */

void sostituteString2(){
    
    char characters[20], temp2, c, r;
    int temp;
    
    for(int i = 0; i < 20; i++){
        printf("Inserisci il carattere n %d \n", i);
        scanf("%c", &characters[i]);
        scanf("%c",&temp2);
    }

    scanf("%c",&temp2);
    printf("Inserisci c\n");
    scanf("%c", &c);

    scanf("%c",&temp2);
    printf("Inserisci r\n");
    scanf("%c", &r);

    for(int i = 0; i < 20; i++){
        if(characters[i] == c){
            characters[i] = r;
        }
        printf("%c", characters[i]);
    }
}

/* ESERCIZIO 7
Chiedere all’utente una sequenza di numeri interi
che termina con l’inserimento dello 0 (e in ogni
caso lunga al massimo 100 elementi). Creare un
array che contenga tutti e soli valori distinti della
sequenza (ovvero omettendo i duplicati).
Visualizzare l’array e il numero di elementi unici
inseriti.
Variante: creare un array che contenga tutti e
soli i valori che nella prima sequenza non
ammettono duplicati. */ 

void selectDistint(){
    
    int A[100], R[100], variante[100], sum = 0, i, j, n, c=0, unique;
    
    for(i = 0; i < 100; i++){
        printf("Inserisci il numero n %d ", i);
        scanf("%d", &n);
        if(n == 0){
            break;
        }
        
        unique = 1;
        for(j = 0; j <= c; j++){
            if(A[j] == n){
                unique = 0;
                R[j] = 0;
                break;
            }
        }

        if(unique){
            A[c] = n;
            R[c] = n;
            c++;
        }

    }

    for(j = 0; j < c; j++){
        printf("%d ", A[j]);
    }
    printf("\n");
    for(j = 0; j < c; j++){
        if(R[j] != 0)
            printf("%d ", R[j]);
    }

}

/* ESERCIZIO 8 
Scrivere un programma che chiede all’utente di
inserire due sequenze di 5 numeri ciascuna
(salvandole in 2 array). Il programma stampa a
video se la seconda sequenza èuna permutazione
della prima.  */

void permutazione(){
    int A[5], B[5], contA = 0, contB = 0, sumA = 0, sumB = 0, prodA = 0, prodB = 0;

    for(int i = 0; i < 5; i++){
        printf("Inserisci il numero n %d ", i);
        scanf("%d", &A[i]);
        sumA += A[i];
        prodA *= A[i];
    }

    printf("Nuova sequenza \n");

    for(int i = 0; i < 5; i++){
        printf("Inserisci il numero n %d ", i);
        scanf("%d", &B[i]);
        sumB += B[i];
        prodB *= B[i];
    }

    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 5; j++){
            if(B[j] == A[i]){
                contA ++;
                break;
            }
        }
    }

    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 5; j++){
           if(A[j] == B[i]){
                contB ++;
                break;
            } 
        }
    }

    printf((contA == contB && contB == 5 && sumA == sumB && prodA == prodB)?"Permutazione":"Non permutazione");
}

int main() {
    
    printf("SELEZIONA L'ESERCIZIO\n");

    int chose = 8;

    if (chose == 0) {
        do {
            printf("INSERISCI 1/2/3/4/5/6/7/8\n");
            scanf("%d", &chose);
        } while (chose > 8 || chose < 1);
    }

    if (chose == 1) {
        fibonacci();
    } else if (chose == 2) {
        numeriPrimi();
    } else if (chose == 3) {
        massimoLocale();
    } else if (chose == 4) {
        pariDispari();
    } else if (chose == 5) {
        sostituteString1();
    } else if (chose == 6) {
        sostituteString2();
    } else if (chose == 7){
        selectDistint();
    } else if (chose == 8){
        permutazione();
    }

    return 0;
};