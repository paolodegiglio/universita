#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#define N 100


void sort(int *p, int *q, int *k){
    int temp;
    if(*p > *q){
        temp = *p;
        *p = *q;
        *q = temp;
    }
    if(*k < *q){
        temp = *k;
        *k = *q;
        *q = temp;
    }
    
    if(*k < *p){
        temp = *k;
        *k = *p;
        *p = temp;
    }
}

void changeOrder(){
    int a,b,c;
    int *p, *q, *k;

    printf("Inserisci a: ");
    scanf("%d",&a);
    printf("Inserisci b: ");
    scanf("%d",&b);
    printf("Inserisci c: ");
    scanf("%d",&c);

    p = &a;
    q = &b;
    k = &c;

    sort(p,q,k);
    printf("%d %d %d \n", a,b,c);
}

int main()
{
    int x,y;
    int *p1;
    int *p2;
    int **pp1;
    int **pp2;
    x = 10;
    y = 20;
    p1 = &x;
    p2 = p1;
    printf("%d %d %d %d\n", x, y, *p1, *p2); // 10, 20, 10, 10
    p1 = &y;
    *p1 = *p1 + *p2;
    printf("%d %d %d %d\n", x, y, *p1, *p2); // 10 30 30 10
    *p1 = *p1 + *p2;
    pp1 = &p2;
    pp2 = pp1;
    *pp2 = *pp1;
   printf("%d %d %d %d\n", x, y, **pp1, **pp2); 
}