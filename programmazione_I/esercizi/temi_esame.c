#include <stdio.h>

/* Scrivi da tastiera una matrice 5*5 e ristampala ruotata di 90 gradi in senso antiorario */
void matrice90(){
    int length = 3;
    int i,j, mat[length][length], newMat[length][length];
    for(i=0; i< length; i++){
        for(j=0; j<length; j++){
            printf("Inserisci la matrice nella posizione [%d][%d] ", i, j);
            scanf("%d", &mat[i][j]);
        }
    }

    for(i=0; i< length; i++){
        for(j=0; j<length; j++){
            printf("%d ", mat[i][j]);
            newMat[length - 1 - j][i] = mat[i][j];
        }
        printf("\n");
    }

    printf("\n");

    for(i=0; i< length; i++){
        for(j=0; j<length; j++){
            printf("%d ", newMat[i][j]);
        }
        printf("\n");
    }
}

/* Prodotto di matrici, il numero di colonne della prima deve essere uguale al numero di righe della seconda */
void prodottoMatrici(){
    int N = 3;
    int m1[3][3] = {{1,2,3}, {4,5,6}, {7,8,9}};
    int m2[3][3] = {{3,6,9}, {2,5,8}, {1,4,7}};
    int m3[N][N];
    int i,j,k;
    int sum;

    for(i = 0; i < N; i++){
        for(j = 0; j < N; j++){
            sum = 0;
            for(k = 0; k < N; k++){
                sum+=m1[i][k] * m2[k][j];
            }
            m3[i][j]=sum;
            printf("%6d", m3[i][j]);
        }
        printf("\n");
    }
}

/* Trova la prima sequenza di k della matrice */
void kSequenza(){
    int lengthX = 5;
    int lengthY = 4;
    int length = 0;
    int startX = 0;
    int startY = 0;
    int started = 0;
    int m1[5][4] = {{3,2,5,4}, {4,4,4,4}, {4,4,3,0}, {0,4,4,6}, {7,3,1,2}};  
    int k = 4;
    int i,j;

    for(i = 0; i < lengthX; i++){
        for(j = 0; j < lengthY; j++){
            printf("%6d", m1[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for(i = 0; i < lengthX; i++){
        for(j = 0; j < lengthY; j++){
            if(m1[i][j] == k){
                if(started == 0){
                    started = 1;
                    startX = i;
                    startY = j;
                } 
                length ++;
            } else if(started == 1){
                break;
            }
        }
    }

    printf("Sequenza lunga %d che inizia in [%d][%d] con k = %d", length, startX, startY, k);
}

/* Dato un array di n interi, far inserire un numero intero x e un y compreso tra 0 e n, verificare se
nell'array ci sono almeno y multipli consecutivi di x */
void arrayMultiple(){
    int n = 10;
    int array[10] = [0,1,2,3,4,5,6,7,8,9,10];
    int x,y,i,sum=0,start,found=0;

    do{
        printf("Inserisci l'intero y compreso tra 0 e 10");
        scanf("%d",&y);
    }while(y > 10 || y <= 1);

    printf("Inserisci x");
    scanf("%d",&x);

    for(i = 0; i < 10; i++){

    }

    for(i = 0; )
}

int main() {
    arrayMultiple();
    return 0;
}