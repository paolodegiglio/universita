#include <stdio.h>
#define N 4

/* 
Scrivere un sottoprogramma C che riceve come parametro una matrice quadrata 4x4 di numeri interi positivi e un intero m. 
Il sottoprogramma calcola e restituisce al chiamante il massimo valore ≤ m contenuto nella matrice se esiste; 
se tale valore non esiste verrà restituito il valore -1 

*/

/**
 * Funzione usata per caricare la matrice
 * 
 * @matrix[N][N] matrice quadrata da caricare con dimensione N 
 */
void caricaMatrice(int matrix[N][N])
{
    int i, j;
    int input, temp;

    printf("Caricamento matrice di dimensione %d \n", N);
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            // Controllo per valori positivi
            do{
                printf("Inserisci il valore in %d %d ", i, j);
                scanf("%d", &matrix[i][j]);
            } while (matrix[i][j] < 0);
        }
    }
}

/**
 * Funzione usata per trovare il valore massimo minore di m
 * 
 * @matrix[N][N] matrice quadrata da caricare con dimensione N 
 * @m valore intero da impostare come limite superiore del valore massimo
 */
int maxFreqValore(int matrix[N][N], int m){
    int max = -1, i, j;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            if(matrix[i][j] <= m && matrix[i][j] >= max){
                max = matrix[i][j];
            }
        }
    }
    return max;
}

int main()
{
    int matrix[N][N];
    int valMax,m;

    /// Caricamento matrice
    caricaMatrice(matrix);

    /// Inserisci il valore di m 
    printf("Inserisci il valore di m ");
    scanf("%d", &m);

    /// Valore con massima frequenza minore di m
    valMax = maxFreqValore(matrix,m);

    /// Stampa valore
    printf("Valore massimo %d ",valMax);

    return 0;
}