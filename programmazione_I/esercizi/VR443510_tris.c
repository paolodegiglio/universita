#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#define NAMELENGTH 100
#define USERSNUMBER 10

typedef struct {
  char name[NAMELENGTH];
  int win;
} User; 

typedef struct {
  User users[USERSNUMBER];
  int length;
} Users;

typedef char Tris[3][3];

// USERS FUNCTION
int indexOfUser(Users*, char[]);
void addUser(Users*);
void showUser(Users*);
void removeUser(Users*);
Users sortUsers(Users*);
void printUsers(Users);
void printUsersOnFile(Users);
int menuAction();

// TRIS FUNCTION
void match(User*, User*);
void manageMatch(Users*);
int diceForStart();
void printTris(Tris);
bool checkForWin(Tris, char);
void resetTris(Tris);

void main(){
  
  Users users = {users: {}, length: 0};
  Users *usersPtr = &users;
  Tris tris;
  Users tempUsers; 
  srand(time(NULL));

  int action;
  
  do {
    resetTris(tris);
    action = menuAction();
    switch(action){
      case(1):
        addUser(usersPtr);
        break;
      case(2):{
        tempUsers = sortUsers(usersPtr);
        printUsers(tempUsers);
        break;
      }
      case(3):
        showUser(usersPtr);
        break;
      case(4):
        removeUser(usersPtr);
        break;
      case(5):{
        tempUsers = sortUsers(usersPtr);
        printUsersOnFile(tempUsers);
        break;
      }
      case(6):{
        manageMatch(usersPtr); 
      }
    };
    printf("\n\n\n");
  } while (action > 0 && action < 7);

  return;
}

/**
 * First ask to take two player
 * Check if the two player exist and are differents
 * Use random for select who goes first 
 *
 * @param usersPtr pointer to users object
 * @return null
 */
void manageMatch(Users* usersPtr){
  int userIndex1, userIndex2;
  if(usersPtr->length < 2){
    printf("Errore non ci sono due giocatori, creane prima qualcuno\n");
  } else {
    bool avaible = false;
    do{
      char name[NAMELENGTH];
      printf("Inserisci il primo utente \n");
      gets(name);
      userIndex1 = indexOfUser(usersPtr, name);

      printf("Inserisci il secondo utente \n");
      gets(name);
      userIndex2 = indexOfUser(usersPtr, name);

      if(userIndex2 == -1 || userIndex1 == -1 || userIndex1 == userIndex2){
        printf("Combinazione utenti non valida, o sono assenti oppure sono lo stesso\n");
        avaible = false;
      } else {
        avaible = true;
      }
    } while(avaible == false);
      
    if(diceForStart() == 1){
      match(&(usersPtr->users[userIndex1]),&(usersPtr->users[userIndex2]));
    } else {
      match(&(usersPtr->users[userIndex2]),&(usersPtr->users[userIndex1]));
    }

  }
}

/**
 * Ask for move
 * Check if the move is avaible
 * Check if someone win
 * Check if there are empty cells for make move
 * Update the win value
 *
 * @return null
 */
void match(User* usersPtr1, User* usersPtr2){
  bool end = false;
  int x1,y1,x2,y2;
  int counter = 0;
  Tris tris;
  resetTris(tris);
  printf("Ordine utenti sorteggiato \n");
  do{
    do {
      printf("%s (O) inserisci le coordinate di x e y (comprese tra 0 e 2 e non occupate)\n", usersPtr1->name);
      scanf("%d %d",&x1,&y1);
    } while (x1 > 2 || y1 > 2 || x1 < 0 || y1 < 0 || tris[x1][y1] != ' ');
    tris[x1][y1] = 'O';   
    if(checkForWin(tris, 'O') == true){
      printf("Ha vinto O\n");
      usersPtr1->win += 1;
      break;
    }
    counter++;
    printTris(tris);

    do {
      printf("%s (X) inserisci le coordinate di x e y (comprese tra 0 e 2 e non occupate)\n", usersPtr2->name);
      scanf("%d %d",&x2,&y2);
    } while (x2 > 2 || y2 > 2 || x2 < 0 || y2 < 0 || tris[x2][y2] != ' ');
    tris[x2][y2] = 'X';    
    if(checkForWin(tris, 'X') == true){
      printf("Ha vinto X\n");
      usersPtr2->win += 1;
      break;
    }
    counter++;
    printTris(tris);
    
    if(counter >= 8){
      printf("PAREGGIO\n");
      end = true;
    }
  } while (end == false);
}

/**
 * Check all the possible combination of win
 */ 
bool checkForWin(Tris tris, char sign){
  if(tris[1][1] == sign && tris[1][2] == sign && tris[1][0] == sign){
    return true;
  }
  if(tris[2][1] == sign && tris[2][2] == sign && tris[2][0] == sign){
    return true;
  }
  if(tris[0][1] == sign && tris[0][2] == sign && tris[0][0] == sign){
    return true;
  }
  if(tris[1][0] == sign && tris[2][0] == sign && tris[0][0] == sign){
    return true;
  }
  if(tris[1][2] == sign && tris[2][2] == sign && tris[0][2] == sign){
    return true;
  }
  if(tris[1][1] == sign && tris[2][1] == sign && tris[0][1] == sign){
    return true;
  }
  if(tris[1][0] == sign && tris[2][2] == sign && tris[0][1] == sign){
    return true;
  }
  if(tris[1][1] == sign && tris[2][2] == sign && tris[0][0] == sign){
    return true;
  }
  return false;
}

/**
 * @return 0 o 1
 */
int diceForStart(){
  int random = rand();
  return (random%2);
}

/**
 * Function used for reset the tris
 * all the cell are set with empty
 * space
 *
 * @param Tris the tris to reset
 * @return null
 */
void resetTris(Tris tris){
  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){
      tris[i][j] = ' ';
    }
  }
};

/**
 * Function used for print all the 
 * cell
 * 
 * @param Tris the tris to show
 * @return null
 */
void printTris(Tris tris){
  for(int i=0; i<3; i++){
    printf("-------------\n|");
    for(int j=0; j<3; j++){
      printf(" %c |",tris[i][j]);
    }
    printf("\n");
  }
  printf("-------------\n");
}

/**
 * Print and ask for the menu
 *
 * @return int value with the selection done
 */
int menuAction(){
  int actionChose;
  printf("BENVENUTO NEL GIOCO DEL TRIS! \nLEGENDA: \n1: inserisci utente \n2: mostra la classifica con tutti gli utenti \n3: Mostra un'utente \n4: elimina un'utente \n5: stampa su file la classifica \n6: inizia una partita\nALTRO: esci dal gioco\n\n");
  scanf("%d", &actionChose);
  char temp;
  scanf("%c",&temp);  
  return actionChose;
};

/**
 * Function used for add if possible
 * a user
 *
 * @param users: users pointer to list
 * @return null
 */ 
void addUser(Users* usersPtr){
  
  bool flag = false;
  char newName[NAMELENGTH]; 
  
  do{
    
    gets(newName);
    int userIndex = indexOfUser(usersPtr, newName);
    
    if(userIndex == -1){
      strcpy(usersPtr->users[usersPtr->length].name, newName);
      usersPtr->users[usersPtr->length].win = 0;
      usersPtr->length ++;
      printf("Utente registrato correttamente \nEcco la nuova lista:\n");
      printUsers(*usersPtr);
      flag = false;
    } else {
      printf("Utente gia' registrato, riprova \n");
      flag = true;
    }
  } while (flag == true);
}

/**
 * Function used for search a user in the array
 * and retrieve the index
 *
 * @param Users pointer to users list
 * @param name name of the new user
 * @return int -1 or real index
 */
int indexOfUser(Users* usersPtr, char name[NAMELENGTH]){
  int x = -1;
  int i;
  Users users = *usersPtr;
  for(i = 0; i < users.length && x == -1; i++){
    if(strcmp(name, users.users[i].name) == 0){
      x = i;
    }
  }
  return x;
}

/**
 * Function used for show a user
 *
 * @param users: users pointer to list
 * @return null
 */ 
void showUser(Users* usersPtr){
  
  bool flag = false;
  char newName[NAMELENGTH]; 
  
  if(usersPtr->length == 0){
    printf("Non ci sono utenti ERRORE\n");
  } else {
    do{
      
      gets(newName);
      int userIndex = indexOfUser(usersPtr, newName);
      
      if(userIndex == -1){
        printf("Utente assente, riprova \n");
        flag = true;
      } else {
        printf("%s vittorie: %d \n", usersPtr->users[userIndex].name, usersPtr->users[userIndex].win);
        flag = false;
      }
    } while (flag == true);
  }
}

/**
 * Function used for remove a user
 *
 * @param users: users pointer to list
 * @return null
 */ 
void removeUser(Users* usersPtr){
  
  bool flag = false;
  char newName[NAMELENGTH]; 
  
  if(usersPtr->length == 0){
    printf("Non ci sono utenti ERRORE\n");
  } else {
    do{
      
      gets(newName);
      int userIndex = indexOfUser(usersPtr, newName);
      
      if(userIndex == -1){
        printf("Utente assente, riprova \n");
        flag = true;
      } else {
        for(int i = userIndex; i < usersPtr->length -1; i++){
          usersPtr->users[i] = usersPtr->users[i+1];
        }
        usersPtr->length --;
        flag = false;
        printf("Utente eliminato\n");
      }
    } while (flag == true);
  }
}

/**
 * Print the users list
 * 
 * @return null
 */
void printUsers(Users users){
  for(int i = 0; i < users.length; i++){
    printf("%s vittorie: %d \n", users.users[i].name, users.users[i].win);
  }
}

/**
 * Print the users list on file
 * 
 * @return null
 */
void printUsersOnFile(Users users){
  FILE * fPtr = NULL;
  fPtr = fopen("trismatch.txt","w");
  if(fPtr == NULL){
    printf("Impossibile creare un file.\n");
    return;
  }

  for(int i = 0; i < users.length; i++){
    fprintf(fPtr, "%s vittorie: %d \n", users.users[i].name, users.users[i].win);
  }
 
  fclose(fPtr);
  printf("Classifica presente in trismatch.txt\n");
  return;
}

/**
 * Sort users
 *
 * @param usersPtr pointer to the users
 * @return the sorted list
 */
Users sortUsers(Users* usersPtr){
  Users users = *usersPtr;
  for(int i = 0; i < users.length; i++){
    for(int j=i+1; j < users.length - 1; j++){
      if(users.users[j].win > users.users[i].win){
        User temp = users.users[j];
        users.users[j] = users.users[i];
        users.users[i] = temp;
      }
    }
  }
  return users;
}

