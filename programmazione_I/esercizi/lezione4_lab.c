#include <stdio.h>

/* Scrivere un programma che ricevuto in ingresso due
numeri interi positivi a e b (se così non è, li richiede),
visualizza un rettangolo di dimensione a*b usando il
carattere '*’ */

void rectangle() {
    int a, b;

    do {
        printf("Inserisci a\n");
        scanf("%d", &a);
    } while (a < 0);

    do {
        printf("Inserisci b\n");
        scanf("%d", &b);
    } while (b < 0);

    for (int i = 0; i < b; i++) {
        for (int z = 0; z < a; z++) {
            printf("* ");
        }
        printf("\n");
    }
}

/* Scrivere un programma che ricevuto in ingresso due
numeri interi positivi a e b (se così non è, li richiede),
visualizza un rettangolo di dimensione a*b usando il
carattere ‘*’ sui bordi e il carattere ‘X’ all’interno.  */

void rectangle2() {
    int a, b;

    do {
        printf("Inserisci a\n");
        scanf("%d", &a);
    } while (a < 0);

    do {
        printf("Inserisci b\n");
        scanf("%d", &b);
    } while (b < 0);

    for (int i = 0; i < b; i++) {
        for (int z = 0; z < a; z++) {
            if (i == 0 || z == 0 || i == (b - 1) || z == (a - 1)) {
                printf("x ");
            } else printf("* ");
        }
        printf("\n");
    }
}

/* Scrivere un programma che ricevuto in ingresso un
numeri interi positivi a (se così non è, lo richiede),
visualizza un quadrato di lato a usando il carattere ‘X’
sulla diagonale principale e il carattere ’*’ altrove. */

void square() {
    int a;

    do {
        printf("Inserisci a\n");
        scanf("%d", &a);
    } while (a < 0);

    for (int i = 1; i <= a; i++) {
        for (int z = 1; z <= a; z++) {
            if (i == z || i == -1 * (z - a - 1)) {
                printf("x ");
            } else printf("* ");
        }
        printf("\n");
    }
}

/* Scrivere un programma che stampa a video la tavola
pitagorica (un quadrato con le tabelline dei numeri da
1 a 10). */

void pitagora() {

    for (int i = 1; i <= 10; i++) {
        for (int z = 1; z <= 10; z++) {
            if (z * i < 10)
                printf(" %d", z * i);
            else
                printf("%d", z * i);
        }
        printf("\n");
    }
}

/* Si definisce triangolare un numero costituito dalla somma dei
primi N numeri interi positivi per un certo N. Esempio: dato
N=4, il numero triangolare Q è 10 (Q=1+2+3+4). Scrivere un
programma che riceva dall’utente un numero intero positivo e
stampi a video se èo meno triangolare (ossia se può essere
scritto come somma dei primi N interi consecutivi).  */

void triangolable() {

    int a, sum = 0, triangolable = 0;

    do {
        printf("Inserisci a\n");
        scanf("%d", &a);
    } while (a < 0);

    for (int i = 1; i <= a; i++) {
        sum += i;
        if (sum == a) {
            triangolable = 1;
            break;
        }
    }
    if (triangolable == 1)
        printf("triangolare\n");
    else
        printf("non triangolare\n");

}

/* Scrivere un programma che acquisisca un indice X tra 0 e 9
(controllare validità del valore inserito) e una sequenza di 10
numeri interi. Il programma dovrà stampare la somma dei
numeri in posizioni minori di X e il prodotto dei numeri in
posizioni successive a X.  */

void indexEs() {
    int index, sum = 0, prod = 1;

    do {
        printf("Inserisci index\n");
        scanf("%d", &index);
    } while (index < 0 || index > 9);

    for (int i = 1; i <= 10; i++) {
        int n;
        printf("Inserisci n\n");
        scanf("%d", &n);
        if(n < i){
            sum += n;
        } else {
            prod *= n;
        }
    }

    printf("somma %d\n", sum);
    printf("prodotto %d", prod);
}

/* Scrivere un programma che inverta la posizione delle cifre di
un numero intero inserito dall’utente (4321 > 1234). Inoltre, il
programma avvisa se il numero inserito è palindromo (si legge
allo stesso modo da sinistra a destra e viceversa, es: 121,
32123, …). */

void invert() {
    int n, invertNum=1, oldn;

    do {
        printf("Inserisci a\n");
        scanf("%d", &n);
    } while (n < 0);
    oldn = n;
    while(n > 0){
        if(invertNum == 1){
            invertNum = n%10;
        } else {
           invertNum = invertNum * 10 + n%10; 
        }
        n/=10;
    }
    printf("%d ",invertNum);
    if(oldn == invertNum){
        printf("palindromo\n");
    }
}

/* Scrivere un programma che, chiesti all'utente due numeri
interi positivi a e b comunica all'utente se si tratta di una
coppia di numeri affiatati oppure no. Definiamo due numeri
affiatati se sono diversi tra loro e la loro somma è pari al
prodotto delle cifre che li compongono. Ad esempio (14, 34) e
(63, 81) sono coppie di numeri affiatati perché
14 + 34 = 1 x 4 x 3 x 4 = 48
63 + 81 = 6 x 3 x 8 x 1 = 144 */

void numberConnected() {
    int a, b, prod = 1;
    do {
        printf("Inserisci a\n");
        scanf("%d", &a);
    } while (a < 0);

    do {
        printf("Inserisci b\n");
        scanf("%d", &b);
    } while (b < 0);

    int sum = a+b;

    while(a > 0){
        prod *= a%10;
        a/=10;
    }

    while(b > 0){
        prod *= b%10;
        b/=10;
    }

    if(prod == sum){
        printf("numeri affiatati");
    } else printf("numeri non affiatati");
}

int main() {

    printf("SELEZIONA L'ESERCIZIO\n");
    printf("1 rettangolo\n");
    printf("2 rectangle2\n");
    printf("3 square\n");
    printf("4 pitagora\n");
    printf("5 triangolable\n");
    printf("6 indexEs\n");
    printf("7 invert\n");
    printf("8 numberConnected\n");

    int chose = 8;

    if (chose == 0) {
        do {
            printf("INSERISCI 1/2/3/4/5/6\n");
            scanf("%d", &chose);
        } while (chose > 6 || chose < 1);
    }

    if (chose == 1) {
        rectangle();
    } else if (chose == 2) {
        rectangle2();
    } else if (chose == 3) {
        square();
    } else if (chose == 4) {
        pitagora();
    } else if (chose == 5) {
        triangolable();
    } else if (chose == 6) {
        indexEs();
    } else if (chose == 7) {
        invert();
    } else if (chose == 8) {
        numberConnected();
    }

    return 0;
};