#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#define N 100

/* -------------------------------------------------------------------------- */
typedef struct {
        int sec;
        int min;
        int hour;
} time;

bool checkTime(time time1);
time insertTime();
void printTime(time time1);
void elapsedTime(time time1, time time2);
void timeEx();

time insertTime(){
    int hour, min, sec;
    time d;
    bool end = false;

    while(!end){ 
        printf("Inserire un tempo uguale o h/m/s.\n");
        scanf("%d/%d/%d", &hour, &min, &sec);
        d.hour = hour;
        d.min = min;
        d.sec = sec;
        end = checkTime(d);
        if(!end){
        printf("Il tempo inserita non e' valido.\n");
        }
    }

    return d; 
}

void printTime(time time1){
    printf("%d:%d:%d\n",time1.hour, time1.min, time1.sec);
}

bool checkTime(time time1){
    if(time1.sec > 60 || time1.sec < 0){
        return false;
    }
    if(time1.min > 60 || time1.min < 0){
        return false;
    }
    if(time1.hour > 24 || time1.hour < 0){
        return false;
    }
    return true;
}

void elapsedTime(time time1, time time2){
    int second1 = time1.hour * 60 * 60 + time1.min * 60 + time1.sec;
    int second2 = time2.hour * 60 * 60 + time2.min * 60 + time2.sec;

    int differencesSecond = abs(second1 - second2);
    printf("%d\n",differencesSecond);
    int hour = differencesSecond/60/60;
    differencesSecond -= hour*60*60;
    int min = differencesSecond/60;
    differencesSecond -= min*60;
    int sec = differencesSecond;
    time time3 = {sec,min,hour};
    printTime(time3);
}

void timeEx(){

    time time1, time2;
    time1 = insertTime();
    time2 = insertTime();

    printTime(time1);
    printTime(time2);
    elapsedTime(time1, time2);
}
/* -------------------------------------------------------------------------- */


/* Si vuole realizzare un’applicazione per la gestione dei voli di una
compagnia aerea. Ogni volo è caratterizzato da un codice (una stringa di
al massimo 5 caratteri), una città di partenza ed una di destinazione
(due stringhe di al massimo 20 caratteri), il costo, il numero massimo di
passeggeri, e il numero di posti liberi per ciascun giorno dell’anno (per
semplicità i giorni dell’anno sono rappresentati con un intero da 0 a
364). Si vogliono gestire un numero variabile di voli (al massimo 50).
All’avvio il programma presenta un menù all’utente e chiede cosa vuole
fare:
0 - inserire i dati di un nuovo volo
1 - visualizzare i voli disponibili
2 - prenotare un volo specificando il codice e il giorno
3 - visualizzare il numero di posti disponibili per un volo, specificati il
codice ed il giorno
4 - cancellare un volo dall’elenco
5 - mostrare tutti voli (con al massimo uno scalo) che collegano due città
6 - uscire
Il programma permette di eseguire la funzionalità richiesta dall’utente,
ed al termine ripresenta il menù. Il programma termina quando viene
specificata l’opzione 6.
• Implementare le strutture dati necessarie per rappresentare i
voli .
• Implementare lo scheletro del programma specificando il codice che
presenta il menù che permette di eseguire la selezione delle varie
funzionalità; lasciare un commento laddove devono essere
implementate le varie funzionalità.
• Implementare la funzionalità 0 stando attenti al fatto che deve esserci ancora spazio in memoria,
• il codice del volo non deve esser già stato utilizzato altrove,
• la città di partenza e destinazione devono essere diverse,
• costo e posti devono essere positivi.
• Implementare la funzionalità 1.
• Implementare la funzionalità 2 verificando se c’è effettivamente
posto sul volo.
• Implementare la funzionalità 3.
• Implementare la funzionalità 4.  */
typedef struct {
    char id[5];
    char start[20];
    char end[20];
    float price;
    int maxSeat;
    int avaibleSeat[365];
} flight;
typedef struct {
    flight flights[50];
    int dim;
} airStation;
int indexOfFlights(airStation station, char id[5]){
    for(int i = 0; i < station.dim; i++){
        if(strcmp(station.flights[i].id,id) == true){
            return i;
        }
    }
    return -1;
}
void addFlight(airStation station){
    char id[5];
    char start[20];
    char end[20];
    float price;
    int maxSeat;
    int index;
    flight flight;
    printf("Inserimento volo\n");
    do{
        printf("Inserisci l'id \n");
        gets(flight.id);
        index = indexOfFlights(station, flight.id);
    } while(strlen(flight.id) >= 5 || strlen(flight.id) <= 0 || index != -1);
    do{
        printf("Inserisci partenza \n");
        gets(flight.start);
    } while(strlen(flight.start) >= 20 || strlen(flight.start) <= 0);
    do{
        printf("Inserisci arrivo \n");
        gets(flight.end);
    } while(strlen(flight.end) >= 20 || strlen(flight.end) <= 0 || strcmp(flight.end, flight.start));
    do{
        printf("Inserisci prezzo \n");
        scanf("%f",&flight.price);
    } while(flight.price <= 0);
    do{
        printf("Inserisci numero posti \n");
        scanf("%d",&flight.maxSeat);
    } while(flight.maxSeat <= 0);
    for(int i = 0; i < 364; station.flights[station.dim].avaibleSeat[i] = flight.maxSeat, i++);
    station.flights[station.dim] = flight;
    station.dim++;
};
void printFl(flight flightIstance){
    printf("%s, partenza: %s, arrivo: %s, costo: %f, numero posti: %d \n", flightIstance.id, flightIstance.start, flightIstance.end, flightIstance.price, flightIstance.maxSeat);
}
void avaibleFlight(airStation station){
    for(int i = 0; i < station.dim; i++){
        printFl(station.flights[i]);
    }
};
void bookFlight(airStation station){
    char id[5];
    int index;
    do{
        printf("Inserisci l'id \n");
        gets(id);
        index = indexOfFlights(station, id);
    } while(strlen(id) >= 5 || strlen(id) <= 0 || index == -1);

    int day;
    do{
        printf("Inserisci giorno 0-364 \n");
        scanf("%d",&day);
    } while(day < 0 || day > 364);

    if(station.flights[index].avaibleSeat[day] >= 1){
        station.flights[index].avaibleSeat[day] --;
        printf("Booked\n");
    } else {
        printf("Non valido\n");
    }
};
void avaibleSeats(airStation station){
    char id[5];
    int index;
    do{
        printf("Inserisci l'id \n");
        gets(id);
        index = indexOfFlights(station, id);
    } while(strlen(id) >= 5 || strlen(id) <= 0 || index == -1);

    int day;
    do{
        printf("Inserisci giorno 0-364 \n");
        scanf("%d",&day);
    } while(day < 0 || day > 364);

    printf("Posti liberi: \n",station.flights[index].avaibleSeat[day]);
};
void deleteFlight(airStation station){
    char id[5];
    int index;
    do{
        printf("Inserisci l'id \n");
        gets(id);
        index = indexOfFlights(station, id);
    } while(strlen(id) >= 5 || strlen(id) <= 0 || index == -1);

    for(int i = index; i < station.dim; i++){
        station.flights[i] = station.flights[i+1];
    }
};
void allFlight(airStation station){
    char start[20];
    char end[20];
    do{
        printf("Inserisci partenza \n");
        gets(start);
    } while(strlen(start) >= 20 || strlen(start) <= 0);
    do{
        printf("Inserisci arrivo \n");
        gets(end);
    } while(strlen(end) >= 20 || strlen(end) <= 0 || strcmp(end, start));

    for(int i = 0; i < station.dim; i++){
        if(strcmp(station.flights[i].start, start)){
            if(strcmp(station.flights[i].end, end)){
                printFl(station.flights[i]);
            }
        }
    }
};
void flightEx(){
    airStation station ={.dim=0};
    int option;
    
    do{
        do{
            printf("0 - inserire i dati di un nuovo volo\n");
            printf("1 - visualizzare i voli disponibili\n");
            printf("2 - prenotare un volo specificando il codice e il giorno\n");
            printf("3 - visualizzare il numero di posti disponibili per un volo, specificati il\n");
            printf("codice ed il giorno\n");
            printf("4 - cancellare un volo dall’elenco\n");
            printf("5 - mostrare tutti voli (con al massimo uno scalo) che collegano due città\n");
            printf("6 - uscire\n\n");
            scanf("%d",&option);
        } while(option < 0 || option > 6);

        switch(option) {

            case 0  :
                addFlight(station);
                break;
            case 1  :
                avaibleFlight(station);
                break;
            case 2  :
                bookFlight(station);
                break;
            case 3  :
                avaibleSeats(station);
                break;
            case 4  :
                deleteFlight(station);
                break;
            case 5  :
                allFlight(station);
                break;
        }

        printf("\n");
    } while(option != 6);
}

int main(){
    //timeEx();
    flightEx();
    return 0;
}