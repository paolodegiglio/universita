#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
/// ABACO
void printAbaco(int num){
  if(num == 0)
    return;
  printf("*");
  printAbaco(num-1);
}
void abaco(int num){
 if(num <= 0){
    printAbaco(num%10);
    return;
  } 
  abaco(num/10);
  printAbaco(num%10);
  printf(" %d\n",num%10);
}

int mainAbaco(){
  abaco(15434123);
  return 1;
}

/// STUDENTI 
typedef struct{
  char name[21];
  int age;
} t_stud;

t_stud * initStudents(int length){
  return (t_stud *) malloc(length*sizeof(t_stud));
}

int mainStudents (){
  t_stud * students = initStudents(10);
  if(students != NULL){
    for(int i = 0; i < 10; i++){
      students[i].age = i;
      strcpy(students[i].name, "i");
    }
  }

  for(int i = 0; i < 10; i++){
    printf("Name: %s, age: %d\n", students[i].name, students[i].age); 
  }
  return 1;
}

/// ARRAY DINAMICO ORDINATO CON INSERIMENTO
void insert(int * array, int * length, int newNumber){
  printf("%d\n", newNumber);
  *length = *length +1;
  int * temp =(int *) realloc(array, (*length)*sizeof(int));
  for(int i = 0; i<*length; i++){
    if(newNumber <= array[i]){
      for(int j=*length-1; j>i; j--){
        array[j]=array[j-1];
      }
      printf("%d\n",i);
      array[i] = newNumber;
      return;
    }
  };
}

int mainArrayDinamico(){
  int length;

  printf("Quanti numeri? ");
  scanf("%d", &length);

  srand(time(NULL));

  int * array = (int *) malloc(length*sizeof(int));
  for(int i = 0; i<length; i++){
    array[i] = rand()%100;
    printf("%d ", array[i]);
  }
  printf("\n");

  for(int i = 0; i < length -1; i++){
    for(int j = i +1; j<length; j++){
      if(array[i] > array[j]){
        int temp = array[j];
        array[j] = array[i];
        array[i] = temp;
      }
    }
  }
 
  for(int i = 0; i<length; i++){
    printf("%d ", array[i]);
  }
  printf("\n");
 
  insert(array, &length, rand()%100);

  printf("\n");
  for(int i = 0; i<length; i++){
    printf("%d ", array[i]);
  }
  printf("\n");

  return 1;
}

/// LISTA STRUTTURE
struct nodo{
  int value;
  struct nodo *next;
}; 

typedef struct nodo elem;

elem * push(elem * lista, int value){
  elem *prec;
  elem *tmp;
  
  tmp = (elem*) malloc(sizeof(elem));
  if(tmp != NULL){
    tmp->value = value;
    tmp->next = NULL;
    if(lista == NULL){
      lista = tmp;
    } else {
      for(prec = lista; prec->next != NULL; prec=prec->next);
      prec->next = tmp;
    }
  } else {
    printf("MEMORIA ESAURITA \n");
  }

  return lista;
}

void visualizza(elem * lista){
  while(lista != NULL){
    printf("%d ",lista->value);
    lista = lista->next;
  };
}

void multiply(elem * lista){
  int value=1;
  int oldValue = 1;
  while(lista != NULL){
    value = lista->value;
    lista->value = oldValue * lista->value;
    lista = lista->next;
    oldValue = value;
  };
}

int sum (elem * lista){
  int s = 0;
  while(lista != NULL){
    s += lista->value; 
    lista = lista->next;
  };
  return s;
}
int main(){
  srand(time(NULL));

  elem* list = NULL;

  int length = rand()%10;
  for(int i = 0; i<length; i++){
    list = push(list, rand()%100);
  }
  
  visualizza(list);

  multiply(list);
  printf("\n");
  visualizza(list);

  return 1;
}
