#include <stdio.h>

int main() {

    /*
        n = numero di righe e colonne
        matrix = matrice di dimensione n
        i = indice di colonna
        j = indice di riga
    */

    int n, i, j;

    // Controllo l'inserimento

    do {
        printf("Inserisci N ovvero il numero di righe e colonne ");
        scanf("%d", &n);
    } while(n < 0);

    // Dichiaro la matrice dopo per avere certezza che n sia definito

    int matrix[n][n];

    printf("\n");

    // Carico la matrice e stampo contemporaneamente la matrice risultante
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            // Ogni cella risulta la moltiplicazione tra riga e colonna, si trova un +1
            // dato che nell'esempio la tabellina partiva da 1 e non 0
            matrix[i][j] = (j+1) * (i+1);
            printf("%5d ", matrix[i][j]);
        }
        printf("\n");
    }

    return 0;
}