#include <stdio.h>

int main() {

   /*
      n: variabile predisposta per l'inserimento del valore
      massimo: variabile predisposta per aggiornare il valore massimo, la predispongo a 18 che so essere il valore minimo inseribile
      minimo: variabile predisposta per aggiornare il valore minimo, la predispongo a 30 che so essere il valore massimo inseribile
      sum: variabile utilizzata per aggiornare la somma dei valori inseriti, fondamentale per restituire la media alla fine
      i: contatore dei numeri validi
   */
   int n, massimo = 18, minimo = 30, sum=0, i=0;

   do{
      // inserimento numero
      printf("Inserisci il voto tra 18 e 30\n");
      scanf("%d",&n);
      // verifico se è valido e aggiorno correttamente i vari valori
      if(n >= 18 && n <= 30){
         if(massimo <= n){
            massimo = n;
         }
         if(minimo >= n){
            minimo = n;
         }
         sum += n;
         i++;
      // Se il valore invalido è il primo stampo il particolare messaggio di errore e termino il programma senza stampare le cose richieste
      } else if(i == 0){
         printf("Non hai inserito neanche un valore valido");
         return 1;
      }
   }
   while(n >= 18 && n <= 30);

   // Stampo i calcoli richiesti
   printf("Media %0.2f, massimo %d, minimo %d, numero di valori inseriti %d", (float) sum/i, massimo, minimo, i);

   return 0;
};