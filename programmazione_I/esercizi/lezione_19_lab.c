#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int matrix[4][4];

void printMatrix(){
  for(int i = 0; i<4; i++){
    for(int j=0; j<4; j++){
      printf("%d ",matrix[i][j]);
    }
    printf("\n");
  }
}

void fillWithRandom(){
  for(int i = 0; i<4; i++){
    for(int j=0; j<4; j++){
      matrix[i][j] = rand()%11;
    }
  }
}

void maxIndex(int* maxI, int* maxJ){
  int sum;
  int sumMax = 0;
  for(int i = 0; i<4; i++){
    sum =0;
    for(int j=0; j<4; j++){
      sum += matrix[i][j];
    }
    if(sumMax < sum){
      sumMax = sum;
      *maxI = i;
    }
  }
  
  sumMax = 0;
  for(int j = 0; j<4; j++){
    sum =0;
    for(int i=0; i<4; i++){
      sum += matrix[i][j];
    }
    if(sumMax < sum){
      sumMax = sum;
      *maxJ = j;
    }
  }

}

int mainMatrix(){
  srand(time(NULL));
  fillWithRandom();
  printMatrix();
  int maxI, maxJ;
  maxIndex(&maxI, &maxJ);
  printf("%d %d", maxI, maxJ);
  return 0;
}

int length(char word[]){
  int cont = 0;
  for(;word[cont] && word[cont] != '\0';cont++);
  return cont;
}

int palindroma(char word[]){
  int count = length(word);
  if(count <= 1) 
    return 1;

  if(word[0] != word[count-1])
    return 0;

  char newWord[80];
  for(int i=1,c=0; i < count - 1; i++,c++){
    newWord[c]=word[i];
  }
  palindroma(newWord);
}

void leggi(char word[]){
  printf("Inserisci la parola\n");
  scanf("%80s",word);
}

int mainLeggi(){
  char word[80];
  leggi(word);
  printf("%d",palindroma(word));
  return 0;
}

int bynarySearch(int value[], int minIndex, int maxIndex, int search){
  if(minIndex == maxIndex && value[minIndex] != search)
    return -1;
  
  if(value[minIndex] > search && value[maxIndex] <search)
    return -1;

  if(value[minIndex] == search){
    return minIndex;
  }

  if(value[maxIndex] == search){
    return maxIndex;
  }

  if(value[minIndex] < search){
    bynarySearch(value,minIndex + 1, maxIndex ,search);
  } else if(value[maxIndex] > search){
    bynarySearch(value,minIndex, maxIndex -1,search);
  }
}

int main(){
  int value[10] = {1,2,3,4,5,5,6,7,8};
  printf("%d",bynarySearch(value, 0, 8,3));
}
