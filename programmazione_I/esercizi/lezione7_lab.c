#include <stdio.h>

/* ESERCIZIO 1
Scrivere un programma che data una matrice int
M[D1][D2] ed un valore intero x calcoli quante
volte x occorre in M. */
void esercizio1(){
    
    int matrix[3][4] = {{1,2,3,4}, {5,6,6,8}, {9,10,11,12}}, x, cont = 0;
    
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 4; j++){
            printf("%d ",matrix[i][j]);
        }
        printf("\n");
    }
    
    printf("Inserisci il numero ");
    scanf("%d", &x);

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 4; j++){
            if(x == matrix[i][j]){
                cont ++;
            }
        }
    }

    printf("\n%d", cont);
}

/* ESERCIZIO 2
Scrivere un programma che data una matrice
quadrata int M[D][D] calcoli la sua trasposta int
N[D][D].
NB: N è la trasposta di M se N[i][j]=M[j][i] per
ogni j∈[0,D-1] e i∈[0,D-1]  */
void esercizio2(){

    int matrix[3][3] = {{1,2,3}, {5,6,7}, {9,10,11}}, x, cont = 0, matrix2[3][3];
    
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            matrix2[j][i]=matrix[i][j];
            printf("%d ",matrix[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            printf("%d ",matrix2[i][j]);
        }
        printf("\n");
    }
}

/* ESERCIZIO 3
Scrivere un programma che memorizza tutte le
righe di una matrice di interi 4x6 in un unico
array di interi lungo 24. */
void esercizio3(){
    int matrix[4][6] = {{1,2,3,4,5,6}, {5,6,7,8,9,10}, {9,10,11,12,13,14}, {1,2,3,4,5,6}};
    int array[24];

    for(int i = 0, z = 0; i < 4; i++){
        for(int j = 0; j < 6; j++, z++){
            array[z]=matrix[i][j];
            printf("%d ",matrix[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for(int i = 0; i < 24; i++){
        printf("%d ",array[i]);
    }
}

/* ESERCIZIO 4
Scrivere un programma che calcoli quanti
elementi in una matrice occorrono almeno due
volte. */
void esercizio4(){

    int matrix[3][4] = {{1,2,3,4}, {4,6,7,8}, {9,10,11,12}};
    int array[12][2] = {{}};

    for(int j = 0; j < 12; j++){
        array[0][j] = 0;
        printf("%d ",array[0][j]);
    }        
        
    printf("\n");

    for(int j = 0; j < 12; j++){
        array[1][j] = 0;
        printf("%d ",array[1][j]);
    }

    printf("\n");

    int flag;
    int lastElement = 0;

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 4; j++){
            flag = 0;
            for(int h = 0; h < 12; h++){
                if(array[1][h] != 0){
                    if(array[0][h] == matrix[i][j]) {
                        flag = 1;
                        printf("Aggiornamento a ++\n");
                        array[1][h] ++;
                        break;
                    }
                }
            }
            if(flag == 0){
                array[0][lastElement] =  matrix[i][j];
                array[1][lastElement] =  1;
                lastElement++;
            }
        }
    }

    printf("\n");

    for(int j = 0; j < 12; j++){
        printf("%d ",array[0][j]);
    }        
        
    printf("\n");

    for(int j = 0; j < 12; j++){
        printf("%d ",array[1][j]);
    }
}

/* ESERCIZIO 5
In un concorso di bruttezza, n giudici esprimono
il loro giudizio su m candidati. Il giudizio èun
valore numerico tra 0 e 5 (più altro il voto più
brutto il candidato). I risultati sono organizzati in
una matrice int R[N][M].
Si scriva un programma in linguaggio C per
determinare il candidato più brutto ed il giudice
più cattivo (ovvero quello che attribuisce i più alti
voti di bruttezza). */
void esercizio5(){
    int matrix[3][4] = {{1,2,3,4}, {5,6,1,8}, {9,10,11,12}};
    int maxGiudice = 0;
    int numeroGiudice = 0;
    int sum;
    for(int i = 0; i < 3; i++){
        sum = 0;
        for(int j = 0; j < 4; j++){
            printf("%d ",matrix[i][j]);
            sum += matrix[i][j];
        }
        if(sum >= maxGiudice){
            maxGiudice = sum;
            numeroGiudice = i+1;
        }
        printf("\n");
    }
    printf("%d numero giudice", numeroGiudice);
}


/* ESERCIZIO 6
Data una matrice quadrata int M[N][N], copiare
tutti i suoi elementi in un vettore lungo N2
secondo l’ordine esemplificato in figura */
void esercizio6(){
    int matrix[6][6] = {{1,2,6,7,15,16}, {3,5,8,14,17,26}, {4,9,13,18,25,27}, {10,12,19,24,28,33}, {11,20,23,29,32,34}, {21,22,30,31,35,36}};
    for(int i = 0; i < 6; i++){
        for(int j = 0; j < 6; j++){
            printf("%4d [%d][%d]    ",matrix[i][j],i,j);
        }
        printf("\n");
    }
    
    printf("\n");

    for(int x = 0; x < 36; x++){
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
            }
        }
    }

    int vet[36] ={};
    int i = 0, j = 0, cont = 0;
    while(i < 6 && j < 6){
        do {
            vet[cont] = matrix[i][j];
            cont++;
            j--;
            i++;
            printf("%d %d\n", j,i);

        } while(j > 0);
        i++;

        do{
            vet[cont] = matrix[i][j];
            cont++;
            j++;
            i--;
            printf("%d %d\n", j,i);
        } while (i > 0);
        j++;
    }

    for(int i = 0; i < 36; i++){
        printf("%2d", vet[i]);
    }

    printf("\n");

}

/* ESERCIZIO 7
Data una matrice int M[D1][D2], si dice che due
elementi M[i][j] e M[p][q] sono adiacenti se
|i-p|=1 oppure |j-q|=1.
Un elemento M[i][j] di M è detto minimo
direzionale se
esistono due elementi M[p][q] e M[r][s],
tali che
1. M[p][q] > M[i][j] < M[r][s]
2. M[p][q] e M[i][j] sono adiacenti
3. M[i][j] e M[r][s] sono adiacenti
Scrivere un programma che calcoli il numero
di minimi direzionali di M */ 
void esercizio7(){
    int matrix[3][4] = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}};
}

int powCustom(int base, int exp){
    int result = base;
    for(int i = 1; i < exp; i++){
        result *= base; 
    }
    return result;
}

int radiceEnnessima(int exp, int num){
    int found = 0;
    int i, potenza;
    for(i = 1; i <= num; i++){
        potenza = powCustom(i, exp);
        printf("Potenza di %d %d: %d\n",i, exp, powCustom(i, exp));

        if(potenza == num){
            return 1;
        }
        if(potenza > num){
            return 0;
        }
    }
    return 0;
}

int main() {
    int result = radiceEnnessima(2, 25);
    printf("\n%d\n",result);
    return 0;
}
    /* int chose = 6;

    if (chose == 0) {
        do {
            printf("INSERISCI 1/2/3/4/5/6/7/8\n");
            scanf("%d", &chose);
        } while (chose > 7 || chose < 1);
    }

    if (chose == 1) {
        esercizio1();
    } else if (chose == 2) {
        esercizio2();
    } else if (chose == 3) {
        esercizio3();
    } else if (chose == 4) {
        esercizio4();
    } else if (chose == 5) {
        esercizio5();
    } else if (chose == 6) {
        esercizio6();
    } else if (chose == 7){
        esercizio7();
    } */

    return 0;
};