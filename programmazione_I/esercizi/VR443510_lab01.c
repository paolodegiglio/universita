#include <stdio.h>

int main() {

    /*
        numbers: array di interi va a contenere i valori inseriti dall'utente
        max: variabile utilizzata per contenere il valore massimo tra i valori inseriti
        i,j: contatori generici utilizzati per caricare o scorrere l'array o il numero di righe
        x: contatore particolare utilizzato per memorizzare il valore della riga nella stampa dell'istogramma
    */

    int numbers[5];
    int max = 0;
    int i;
    int j;
    int x;

    // Caricamento array, durante il caricamento trovo il valore massimo
    for(i = 0; i < 5; i++){
        // Controllo sull'input
        do {
            printf("Inserisci il numero n %d, ricordati che deve essere positivo ", i);
            scanf("%d", &numbers[i]);
        } while (numbers[i] < 1);

        // Aggiorno il massimo
        if(max <= numbers[i]){
            max = numbers[i];
        }
    }

    // Preparo un ciclo lungo quanto il valore maggiore tra i numeri
    for(i = 0, x = max; i < max; i++, x--){
        // Itero l'array e se il valore dell'array risulta maggiore rispetto a quello della riga lo stampo utilizzo una variabile x per poter avere sempre il valore della riga dell'istogramma
        for(j = 0; j < 5; j++){
            if(numbers[j] >= x){
                printf("*");
            } else printf(" ");
        }
        // Vado a capo dopo aver stampato la riga
        printf("\n");
    }

    return 0;
}