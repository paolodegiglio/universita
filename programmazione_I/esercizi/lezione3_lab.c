#include <stdio.h>

/* ES1 Scrivere un programma C che classifica un
carattere immesso da tastiera come: alfabetico 
(az oppure A-Z), cifra (0-9), speciale 
(tutti gli altri). */

void characher(){
    
    char input;
    scanf("%c",&input);
    printf("Inserisci il carattere\n");
    scanf("%c",&input);

    int tempChar = (int) input;
    

    if(tempChar >= 48 && tempChar <= 57){
        printf("Numero\n");
    } else if (tempChar >= 65 && tempChar <= 90){
        printf("Carattere maiuscolo\n");
    } else if(tempChar >= 97 && tempChar <= 122){
        printf("Carattere minuscolo\n");
    } else { 
        printf("Carattere speciale\n");
    }
}

/* ES2 Scrivere un programma in C che richiede
all’utente di inserire i coefficienti a, b, c di un
polinomio di secondo grado ax2 + bx + c = 0. Se a è
pari a zero, il programma informa che il polinomio
è di grado inferiore al secondo. In caso contrario, il
programma calcola il determinante (det)
dell’equazione di secondo grado e informa l’utente
se le soluzioni sono reali e distinte (det>0),
coincidenti (det=0) o complesse coniugate (det<0). */

void equations(){
    
    float a,b,c;

    printf("Inserisci a\n");
    scanf("%f",&a);

    if(a == 0){
        printf("Equazione di primo grado\n");
    } else {
        printf("Inserisci b\n");
        scanf("%f",&b);
        printf("Inserisci c\n");
        scanf("%f",&c);  
        float det = (b*b) - (4*a*c);
        if(det == 0){
            printf("Soluzioni coincidenti\n");
        } else if(det > 0){
            printf("Soluzioni reali coincidenti\n");
        } else {
            printf("Complesse coniugate\n");
        }
    }

}

/* ES3 Scrivere un programma in C che acquisisce tre
numeri. Il programma verifica se i tre numeri
possono rappresentare le dimensioni dei lati di un
triangolo: devono essere valori positivi e la somma
di due numeri deve essere maggiore del terzo. In
caso il controllo fallisca, il programma deve
stampare un apposito messaggio di errore. Se il
controllo è stato superato con successo, il
programma stabilisce che tipo di triangolo è
(isoscele, equilatero o scaleno) e stampa un
apposito messaggio a video. */

void triangle(){
    float a,b,c;

    printf("Inserisci a\n");
    scanf("%f",&a);
    printf("Inserisci b\n");
    scanf("%f",&b);
    printf("Inserisci c\n");
    scanf("%f",&c);  
    
    if((((a+b)>c) || ((a+c)>b) || ((c+b)>a)) && (a>0 && b>0 && c>0)){
        printf("TRIANGOLO");
        if((a==b) && (b==c)){
            printf(" EQUILATERO\n");
        } else if((a==b) || (b==c) || (a==c)){
            printf(" ISOSCELE\n");
        } else {
            printf(" SCALENO\n");
        }
    } else {
        printf("NON TRIANGOLO");
    }
    
}

/* ES4 Scrivere un programma C che chiede all’utente
due numeri interi n1 e n2 e un carattere op
(appartenente all’insieme {‘+’,’-’,’*’,’/’}). Il
programma calcola e stampa a video il risultato
dell’operazione corrispondente a op applicata ai
numeri n1 e n2. */

void calculator(){
    char input;
    scanf("%c",&input);
    printf("Inserisci l'operatore\n");
    scanf("%c",&input);
    int asciiInput = (int) input;
    float a,b;

    printf("Inserisci a\n");
    scanf("%f",&a);
    printf("Inserisci b\n");
    scanf("%f",&b);
    
    switch(asciiInput) {

        case 42  :
            printf("%.2f", a*b);
            break; 
            
        case 43  :
            printf("%.2f", a+b);
            break;
        case 45  :
            printf("%.2f", a-b);            
            break;
        case 47  :
            printf("%.2f", a/b);            
            break;
        
        default :
            printf("Non hai inserito un operatore valido");
    }
}

/* ES5 Scrivere un programma in C che richiede
all’utente di inserire ore e minuti, calcola l’ora a
San Francisco (9 ore indietro) e a Teheran (+3:30)
e la stampa a video in formato «0-24 h : min». Se a
Teheran è già il giorno successivo o a San
Francisco ancora quello precedente, il programma
corregge l’ora e avvisa l’utente con un opportuno
messaggio a schermo. */

void parserDate(int hour,int min){
    if(hour < 10){
        printf("0%d : ", hour);
    } else {
        printf("%d : ", hour);
    }

    if(min < 10){
        printf("0%d\n", min);
    } else {
        printf("%d\n", min);
    }
}

void hour(){
    
    int hour, min;

    do {
        printf("Inserisci ore\n");
        scanf("%d",&hour);
    } while (hour > 23 || hour < 0);

    do {
        printf("Inserisci minuti\n");
        scanf("%d",&min);
    } while (min > 59 || min < 0);
    
    // Formattatore per rendere l'orario con formato hh:mm
    parserDate(hour, min);
    
    int hourT = hour, minT = min;

    // Controllo se si passa al giorno dopo se no mi limito ad aggiungere

    if((minT + 30) > 59){
        hourT++;
        minT = (minT + 30) - 60;
    } else {
        minT += 30;
    }

    if((hourT + 3) > 23){
        hourT = (hourT + 3) - 24;
    } else {
        hourT += 3;
    }

    // Formattatore per rendere l'orario con formato hh:mm
    parserDate(hourT, minT);    

    int hourS = hour, minS = min;

    // Controllo se si passa al giorno dopo se no mi limito ad aggiungere

    if((hourS - 9) < 0){
        hourS = (hourS - 9) + 24;
    } else {
        hourS -= 9;
    }

    parserDate(hourS, minS);

}

/* ES6 Su una scacchiera 8x8 sono posizionati due pezzi:
il Re bianco e la Regina nera. Si scriva un
programma in linguaggio C che acquisisce le
posizioni del Re e della Regina in termini di
coordinate (x,y) assumendo che la posizione (1,1)
sia situata in basso a sinistra rispetto al giocatore.
Il programma controlla prima che le coordinate
inserite siano valide; in particolare entrambi i
pezzi devono trovarsi all’interno della scacchiera
ed inoltre non possono trovarsi nella stessa
posizione. In seguito il programma determina se
la Regina è in posizione tale da poter mangiare il
Re e visualizza un apposito messaggio
specificando anche in che direzione e per quante
posizioni deve muoversi per mangiare. */

void chess(){
    int queenX, queenY, kingX, kingY;

    // Controllo se hanno la stessa posizione
    do{

        printf("La posizione del re e della regina non devono essere uguali\n");

        // Controllo la posizione della regina
        do {
            printf("X REGINA {1 .. 8}\n");
            scanf("%d", &queenX);
        } while (queenX > 8 || queenX < 1);
        
        do {
            printf("Y REGINA {1 .. 8}\n");
            scanf("%d", &queenY);
        } while (queenY > 8 || queenY < 1);

        // Controllo la posizione del re
        do {
            printf("X RE {1 .. 8, reg}\n");
            scanf("%d", &kingX);
        } while (kingX > 8 || kingX < 1);
        
        do {
            printf("Y RE {1 .. 8, regY}\n");
            scanf("%d", &kingY);
        } while (kingY > 8 || kingY < 1);

    } while (kingY == queenY && kingX == queenX);

    // Controllo sulla mossa orizzontale
    if(kingY == queenY){
        if((kingX - queenX) > 0)
            printf("La regina mangia e si deve muovere di %d verso destra ", (kingX - queenX));
        if((kingX - queenX) < 0)
            printf("La regina mangia e si deve muovere di %d verso sinistra ", (queenX - kingX));
    } else if(kingX == queenX){
        // Controllo sulla mossa verticale
        if((kingY - queenY) > 0)
            printf("La regina mangia e si deve muovere di %d verso l'alto ", (kingY - queenY));
        if((kingY - queenY) < 0)
            printf("La regina mangia e si deve muovere di %d verso il basso ", (queenY - kingY));
    } else if((kingY - queenY) == (kingX - queenX)){
        // Controllo sulla mossa obliqua
        printf("La regina mangia in obliquo andando spostandosi di ");
        if((kingY - queenY) > 0)
            printf("%d verso alto", (kingY - queenY));
        if((kingY - queenY) < 0)
            printf("%d verso basso", (queenY - kingY));
        if((kingX - queenX) > 0)
            printf(" e di %d verso destra", (kingX - queenX));
        if((kingX - queenX) < 0)
            printf(" e di %d verso sinistra", (queenX - kingX));
    } else {
        printf("La regina non mangia");
    }
}

int main() {

    printf("SELEZIONA L'ESERCIZIO\n");
    printf("1 tipo di carattere\n");
    printf("2 equazioni secondo grado\n");
    printf("3 paragone triangolo dati i tre lati\n");
    printf("4 calcolatrice con operatore inserito dall'utente\n");
    printf("5 orario Theran e San Francisco\n");
    printf("6 scacchiera\n");

    int chose;

    /* do {
        printf("INSERISCI 1/2/3/4/5/6\n");
        scanf("%d", &chose);
    } while (chose > 6 || chose < 1); */

    chose = 6;

    if (chose == 1) {
        characher();
    } else if (chose == 2) {
        equations();
    } else if (chose == 3) {
        triangle();
    } else if (chose == 4) {
        calculator();
    } else if (chose == 5) {
        hour();
    } else if (chose == 6) {
        chess();
    }

    return 0;
};