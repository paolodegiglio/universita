#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 100

/* Scrivere un programma che acquisisce due stringhe s1
ed s2 di al massimo 100 caratteri ciascuna. Il
programma verifica se s2 è presente in s1 e tutte le
volte che compare sostituisce il primo carattere di s2
in s1 con il carattere X (è ammesso l’uso solo di strlen) */
void substring(){
    char s1[N];
    char s2[N];
    
    gets(s1);  
    printf("%s\n",s1);

    gets(s2);
    printf("%s\n",s2);

    for(int i = 0; i < strlen(s1); i++){

        int found = 1;
        for(int j = 0; j < strlen(s2) && found == 1; j++){
            if(s2[j] != s1[i+j]){
                found = 0;
            }
        }
        if(found == 1)
            s1[i] = 'X';

    }    

    printf("%s\n",s1);
}

/* Scrivere un programma che chiede in input una
stringa (senza spazi) di al massimo 50 caratteri e
verifica che la stringa sia ben parentesizzata. Una
stringa èben parentesizzata se le parentesi (che
possono essere solo tonde) sono chiuse correttamente
nell’ordine in cui vengono aperte. Esempio di stringa
ben parentesizzata: bla(bla)(bla(bla))(bla()(bla)())   */
void parentesi(){
    char s1[N];
    gets(s1);
    int ordinata = 1;
    for(int i = 0; i < strlen(s1); i++){
        if(s1[i] == '('){
            ordinata = 0;
            for(int j = i; j<strlen(s1) && ordinata == 0; j++){
                if(s1[j] == ')'){
                    s1[j] = 'x';
                    ordinata = 1;
                }
            }
        }
        if(s1[i] == ')'){
            ordinata = 0;
        }
    }
    printf("%d", ordinata);
}

/* Scrivere un programma che implementi il gioco
dell’impiccato. Il programma chiede al primo giocatore
di inserire una parola di lunghezza massima 10
caratteri. Il secondo giocatore ha a disposizione 10
tentativi per indovinare la parola. Ad ogni tentativo, il
programma chiede all’utente di inserire una lettera e
visualizza i tentativi ancora disponibili. Inoltre, viene
visualizzata la parola in modalità «nascosta» (es.
«ciao» diventa «****»); se vengono indovinate delle
lettere, il programma le visualizza nella posizione
corretta (es. se inserisco «a» la parola nascosta diventa
«**a*»).
Il programma termina quando il giocatore indovina la
parola oppure quando terminano i tentativi a
disposizione. */
int sameString(char s1[N], char s2[N]){
    for(int i = 0; i <strlen(s1); i++){
        if(s1[i] != s2[i]){
            return 0;
        }
    }
    return 1;
}
void impiccato(){
    char s1[N];
    char s2[N];
    char showed[N];
    gets(s1);
    for(int i =0; i<strlen(s1); i++){
        showed[i] = '*';
    }
    int vite = 10;
    int vittoria = 0;

    for(int i = 0; i<vite && vittoria == 0; i++){
        gets(s2);
        if(strlen(s2) == 1){
            for(int j = 0; j<strlen(s1); j++){
                if(s2[0] == s1[j]){
                    showed[j] = s2[0];
                }
            }
        } else if(strlen(s2) == strlen(s1)){
            vittoria = sameString(s1,s2);
            printf("vittora");
            break;
        }
        printf("%s vite: %d\n", showed, 9 - i);
    }
}

/* ○ Scrivere un programma in linguaggio C che legga una frase introdotta
da tastiera. La frase è terminata dall’introduzione del carattere di
invio. La frase contiene sia caratteri maiuscoli che caratteri minuscoli,
e complessivamente al più 100 caratteri. Il programma deve svolgere
le seguenti operazioni:
● visualizzare la frase inserita
● costruire una nuova frase in cui il primo carattere di ciascuna
parola nella frase di partenza è stato reso maiuscolo. Tutti gli altri
caratteri devono essere resi minuscoli. Il programma deve
memorizzare la nuova frase in una opportuna variabile
● visualizzare la nuova frase.
Ad esempio la frase cHe bElLA gIOrnaTa diviene Che Bella
Giornata  */
void capitalize(){
    char s1[N];
    char temp;
    int count = 0;
    do{
        scanf("%s",s1[count]);
        printf("%d\n", s1[count]);
        count++;
    } while(count <= N);
}

int main(){
    return 0;
}