#include <stdio.h>
#define PI 3.14
#define costBottle 0.4

void gradiRadianti() {
    float angle;
    printf("Inserisci l'angolo in gradi\n");
    scanf("%f",&angle);
    printf("Angolo in radianti: %.2f ", angle * PI / 180);
};

void convKm() {
    float kmH;

    do{
        printf("Inserisci i km/h, devono essere maggiori di 0\n");
        scanf("%f",&kmH);
    } while(kmH <= 0);

    printf("Metri al secondo: %.2f ", kmH / 3.6);
    printf("\nMiglia orarie: %.2f ", kmH * 0.64);
};

void bottle(){

    float fMoney;

    do{
        printf("Inserisci i tuoi soldi\n");
        scanf("%f",&fMoney);
    } while(fMoney <= 0);

    int iMoney = (fMoney * 100);

    int nBottle = 0;

    while(iMoney>0){
        if(iMoney >= 40){
            nBottle ++;
            iMoney -= 40;
        } else {
            printf("%d \n",nBottle);
            if(iMoney >= 20){
                iMoney -= 20;
                printf("1*20 ");
            } else if(iMoney >= 5){
                printf("%d*5 ",  (iMoney / 5 ));
                iMoney = iMoney - (iMoney / 5 * 5);
            } else if(iMoney >= 2){
                printf("%d*2 ", iMoney / 2);
                iMoney = iMoney - (iMoney / 2 * 2);
            } else {
                printf("%d*1 ",iMoney);
                iMoney --;
            }

        }
    }
}

int main() {

    printf("SELEZIONA L'ESERCIZIO\n");
    printf("1 esercizio conversione da gradi a radianti\n");
    printf("2 esercizio conversione da km a miglia\n");
    printf("3 esercizio bottiglietta\n");

    int chose;

    do {
        printf("INSERISCI 1/2/3\n");
        scanf("%d", &chose);
    } while (chose != 1 && chose != 2 && chose != 3);

    if (chose == 1) {
        gradiRadianti();
    } else if (chose == 2) {
        convKm();
    } else {
        bottle();
    }

    return 0;
};