#include <stdio.h>

/* Scrivere un programma C che stampa in sequenza i numeri da 1 a 1000 separati da una virgola, sostituendo con "CIP" i numeri che sono multipli di 3, con "CIOP" i numeri che sono multipli di 7 e con "CIPCIOP" i numeri che sono multipli sia di 3 che di 7. 
* ES: 1, 2, CIP, 4, 5, CIP, CIOP, 8, CIP, 10, 11, CIP, 13, CIOP, CIP, 16, 17, CIP, 19, 20, CIPCIOP, 22, 23, CIP, 25, 26, CIP, CIOP, 29, CIP, 31, 32, CIP, 34, CIOP, CIP, 37, 38, CIP, 40, 41, CIPCIOP, 43, 44 [ ... ] 
NB: Consegnate solo il codice sorgente con estensione .c con il nome con la seguente forma:
<numero di matricola>_es004.c

(ES: VR3XXXXXX_es004.c) */

int main() {

    /*
        i = contatore
    */

    int i;

    for(i = 1; i < 1000; i++){

        if(i % 3 == 0 && i % 7 == 0){
            printf(" CIPCIOP");
        } else if(i % 3 == 0){
            printf(" CIP");
        } else if(i % 7 == 0){
            printf(" CIOP");
        } else {
            printf(" %d",i);
        }

        // Metto la virgola alla fine e controllo che non sia l'ultimo valore per non metterlo
        if(i != 999){
            printf(",");
        }
    }

    return 0;
}