#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/** int ricorsiva(int n){
  *   if(n == 0)
  *     return 0;
  *   return n+ricorsiva(n-1);
  * }
  *
  * double f(double a, int n){
  *   if(n == 0)
  *     return 0;
  *   return (a - n/a) + f(a, n-1);
  * }
  *
  * int checkDiv(int x,int y,int start){
  *   if(x % start == 0 && y % start == 0){
  *     return 0;
  *   }
  *
  *   if(start <= x && start <= y){
  *     return checkDiv(x,y,start+1);
  *   }
  *
  *   return 1;
  * } */

float leggi(){
  float res;
  do {
    printf("Leggi il numero\n");
    scanf("%f",&res);
  } while (res < 0);

  return res;
}

float quoziente(float x,float y){
  if(x<y){
    return 0;
  }

  return 1+((x-y)/y);
}

int quozienteMain(){
    float x = leggi();
    float y = leggi();
    printf("%f %f\n",x,y);
    printf("%f",quoziente(x,y));
    return 0;
}

/** int cifraK(int num, int k){
  *   if(k == 0){
  *     return 0;
  *   }
  *   double powN = pow(10,k);
  *   printf("%d\n",powN);
  *   num = num%(int)powN;
  *   return num + cifraK(num,k-1);
  * } */

int sommaBuffa(int n){
  if(n <= 3)
    return 1;

  return sommaBuffa(n-1) + sommaBuffa(n-3);
}

int sommaArray(int array[10], int dim, int i){
  if(i == dim){
    return 0;
  }else  
  return array[i]+ sommaArray(array, dim, i +1);
}

void stampaRicorsiva(int num){
  if(num == 0){
    return;
  } else {
    stampaRicorsiva(num/10);
    printf("%d ", num%10);
  }
}

int cifraMassima(int num, int max){
  if(num == 0){
    return 0;
  } else {
    max = cifraMassima(num/10, max);
    if(max < num%10){
      max = num%10;
    }
    return max;
  }
}

void stampa(int num){
  if(num == 0){
    printf("Zero");
    return;
  } else{
    int max = cifraMassima(num, 0);
    printf("%d", max);
  }
}


int main(){
  int num = (int)leggi();
  stampa(num);
  return 0;
}
