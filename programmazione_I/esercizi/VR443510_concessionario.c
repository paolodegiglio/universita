#include<stdio.h>
#include<time.h>
#include<string.h>

/// CODICE ASSEGNATO
#define N_CONCESSIONARI 10
#define MAX_STR 30
#define MAX_TRG 7
#define MAX_AUTO_CONCE 50

typedef struct{
  char modello[MAX_STR+1], targa[MAX_TRG+1];
  int meseImmatricolazione, annoImmatricolazione;
} t_auto;

typedef struct{
  char piva[MAX_STR+1];
  char nome[MAX_STR+1], cognome[MAX_STR+1];
} t_persona;

typedef struct{
  int codiceConcessionario;
  t_persona gestore;
  // Modifica effettuata nella struttura dato che auto sembra essere riservata
  t_auto macchine[MAX_AUTO_CONCE];
  int nAuto; /* numero effettivo auto nel concessionario*/

} t_concessionario;
/// FINE CODICE ASSEGNATO

/// CODICE CUSTOM
typedef char string[MAX_STR];

typedef struct{
  int length;
  string stringhe[100];
} fake_string;

fake_string nomi = {
  length: 3,
  stringhe: {"Paolo","Luca","Davide"}
};

fake_string cognomi = {
  length: 3,
  stringhe: {"Rossi","Verdi","Neri"}
};

fake_string piva = {
  length: 3,
  stringhe: {"IT1233453453","IT3424234234","IT1234322314"}
};

fake_string targhe = {
  length: 3,
  stringhe: {"DW1233LE","DW121J12","DDED212E"}
};

fake_string modelli = {
  length: 3,
  stringhe: {"PANDA","PUNTO","FERRARI"}
};

// Carica con valori casuali i concessionari
void fakerLoader(t_concessionario concessionari[N_CONCESSIONARI]){
  for(int i =0; i<10; i++){
    concessionari[i].codiceConcessionario = i;
    int random;

    // Inizializza nome casuale
    random = rand()%nomi.length;
    strcpy(concessionari[i].gestore.nome,nomi.stringhe[random]);

    // Inizializza cognome casuale
    random = rand()%cognomi.length;
    strcpy(concessionari[i].gestore.cognome,cognomi.stringhe[random]);

    // Inizializza piva casuale
    random = rand()%piva.length;
    strcpy(concessionari[i].gestore.piva,piva.stringhe[random]);

    for(int j=0; j<MAX_AUTO_CONCE; j++){
      // Inizializza mese e anno
      concessionari[i].macchine[j].meseImmatricolazione = rand()%11+1; 
      concessionari[i].macchine[j].annoImmatricolazione = rand()%10+2010; 
      
      // Inizializza modello casuale
      random = rand()%modelli.length;
      strcpy(concessionari[i].macchine[j].modello,modelli.stringhe[random]);
    
      // Inizializza targa casuale
      random = rand()%targhe.length;
      strcpy(concessionari[i].macchine[j].targa,targhe.stringhe[random]);
    }
  }
}

// Stampa le auto immatricolate nell'anno specificatamente richiesto per ognuno dei concessionari
void printer(t_concessionario concessionari[N_CONCESSIONARI], int anno){
  for(int i=0; i<10; i++){
    printf("Conc. %d, codice %d: gestore %s %s \n",i,concessionari[i].codiceConcessionario, concessionari[i].gestore.nome, concessionari[i].gestore.cognome);
    printf("Immatricolazioni %d:\n",anno);
    for(int j=0; j<MAX_AUTO_CONCE; j++){
      if(concessionari[i].macchine[j].annoImmatricolazione == anno){
        printf("* mese %d: %s, %s \n",concessionari[i].macchine[j].meseImmatricolazione,concessionari[i].macchine[j].modello,concessionari[i].macchine[j].targa);
      }
    }
  }
}

int main(){
  srand(time(NULL));
  t_concessionario concessionari[N_CONCESSIONARI];
  fakerLoader(concessionari); 
  printer(concessionari, 2015);
}
