#include <stdio.h>
#include <time.h>
#include <stdlib.h>

/**
 * Scrivere un programma in linguaggio C che legga da tastiera una 
 * sequenza di numeri positivi e ad ogni numero letto ne stampi 
 * la somma progressiva. Il programma termina quando si introduce 
 * un numero minore o uguale a zero.
 */
void sommaProg()
{
    int i, sum = 0, n;

    do
    {
        printf("\nInserisci n ");
        scanf("%d", &n);
        if (n > 0)
        {
            sum += n;
            printf("\nSum: %d", sum);
        }
    } while (n > 0);

    return;
}

/**
 * Scrivere un programma in linguaggio C che legga da tastiera una sequenza di lunghezza ignota a priori di numeri interi positivi. 
 * Il programma, a partire dal primo numero introdotto, stampa ogni volta la media di tutti i numeri introdotti.
 * Terminare quando il numero inserito è negativo. 
 */
void mediaProg()
{
    int i = 0, sum = 0, n;
    float media;

    do
    {
        printf("\nInserisci n ");
        scanf("%d", &n);
        if (n >= 0)
        {
            sum += n;
            i++;
        }
    } while (n >= 0);
    printf("\nSum: %d, nums: %d", sum, i);
    media = (float)(sum) / i;
    printf("\nMedia: %f", media);

    return;
}

/**
 * Si scriva un programma in linguaggio C che legga da tastiera un numero intero A, 
 * lo riduca ad un valore compreso tra 0 e 127 mediante sottrazione ripetuta di un 
 * adeguato numero di volte del valore 128 (non si utilizzi l’operatore modulo o and), 
 * lo interpreti come caratteri ASCII e lo stampi sul video. 
 */
void asciiConverter()
{
    int n;
    char x;

    printf("\nInserisci n ");
    scanf("%d", &n);
    n *= (n > 0) ? 1 : -1;

    while (n > 127)
    {
        n -= 128;
    }
    printf("%d", n);
    x = n;
    printf("%c", x);

    return;
}

/**
 * Generare un numero a caso e chiedere all'utente un numero fino a quando non e' uguale a quello generato casualmente. 
 * Dire ogni volta se il numero immesso e' > o < di quello iniziale. 
 * Per generare un numero a caso invocare la funzione rand(). Ad esempio se r è una variabile intera    
 * r = rand();assegna a r un valore casuale.
 */
void randNum()
{
    int r, num;

    r = (rand() % 10) + 1;

    do
    {
        printf("\nInserisci n ");
        scanf("%d", &num);
        if (num != r)
        {
            printf((num > r) ? ">\n" : "<\n");
        }
    } while (num != r);

    printf("\n%d", r);
}

/**
 * Scrivere un programma in linguaggio C per la rappresentazione del triangolo di Floyd. 
 * Il triangolo di Floyd è un triangolo rettangolo che contiene numeri naturali, definito 
 * riempiendo le righe del triangolo con numeri consecutivi e partendo da 1 nell’angolo in alto a sinistra. 
 * Si consideri ad esempio il caso N=5. 
 * Il triangolo di Floyd e’ il seguente: 
 * 1 
 * 2 3
 * 4 5 6 
 * 7 8 9 10 
 * 11 12 13 14 15
 */
void floyd()
{
    int n = 10, c = 1;
    int i, j;

    for (i = 1; i <= n; i++)
    {
        for (j = 0; j < i; j++)
        {
            printf("%5d", c);
            c++;
        }
        printf("\n");
    }
}

/**
 * Caricamento in un array max di 30 lunghezza 
 */
int caricaArray(int vet[])
{
    int num;

    do
    {
        printf("\nInserisci n ");
        scanf("%d", &num);
    } while (num >= 30 || num <= 0);

    for (int i = 0; i < num; i++)
    {
        printf("\nInserisci valore %d ", i);
        scanf("%d", &vet[i]);
    }

    return num;
}

/**
 * Controlla se l'array è tutto uguale per i suoi valori
 */
void checkEqual(int vet[], int length)
{

    for (int i = 1; i < length; i++)
    {
        if (vet[i] != vet[i - 1])
        {
            printf("Diverso");
            return;
        }
    }

    printf("Uguale");
    return;
}

/**
 * Index of function
 */
int indexOf(int vet[], int length, int num)
{
    for (int i = 0; i < length; i++)
    {
        if (vet[i] == num)
        {
            return i;
        }
    }

    return -1;
}

int main()
{

    srand(time(NULL));

    /* int vet[30];
    int length = caricaArray(vet);
    int num = 5;
    printf("\n %d", indexOf(vet, length, 5)); */
    //floyd();
    //randNum();
    //asciiConverter();
    //mediaProg();
    //sommaProg();
    return 0;
}