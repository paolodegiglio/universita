/**
 * Compilare il programma con gcc aggiungendo alla fine -lm
 */
#include <stdio.h>
#include <math.h>

/*
 * Ritorna la distanza tra due punti
 */
double distance(double xs, double ys, int i, int j) {
    return sqrt(xs*xs + ys*ys);
}

/**
 * Restituisce il numero dei punti la cui distanza dall'origine
 * degli assi è minore o uguale al valore indicato,
 * utilizzando la funzione distance().
 */
int count_in(double xs[], double ys[], int size, double radius) {
    int i;
    int result = 0;
    for(i = 0; i < size; i++){
        if(distance(xs[i], ys[i], 0, 0) <= radius){
            result ++;
        }
    }
    return result; 
}

/**
 * Stampa la sequenza dei punti 
 */
void print(double xs[], double ys[], int size) {
    int i;
    for(i = 0; i < size; i++){
        printf("(%.2f, %.2f)\n", xs[i], ys[i]);
    }
}

/*
 * Program entry point
 */
int main(){
    double xs[] = {0, -1.1, 2, 0.9};  // Ascisse
    double ys[] = {0.5, 0, 1, 0};  // Ordinate
    int size = 6;
    double radius = 1;

    print(xs, ys, size);
    printf("Numero di punti dentro il cerchio di raggio %.2lf centrato nell'origine: %i\n", radius, count_in(xs, ys, size, radius));
    return 0;
}
