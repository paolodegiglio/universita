#include <stdio.h>
#include <stdlib.h>
#define N 2
#define M 3
#include <time.h>

/*
 ulteriori direttive di pre-processing
*/

int main(void) {
  /*
  dichiarazioni
  matrix: matrice richiesta
  i,j: contatori per muovermi nella matrice e nell'array
  filter: array richiesto
  result: array richiesto per risultato
  */
  int matrix[N][M];
  int i,j;
  float filter[M];
  float result[M];
  
  srand(time(NULL));

  /*
  inizializzazione Matrix:
  */

  for(i=0; i<N; i++){

    for(j=0; j<M; j++){ 
      if(i % 2 == 0){
        // Situazione pari
        do{
          matrix[i][j] = rand() % 10;
        } while(matrix[i][j] % 2 == 1);

      } else {
        // Situazione dispari
        do{
          matrix[i][j] = rand() % 10;
        } while(matrix[i][j] % 2 == 0);
      }
    }

  }

  /*
  inserimento valori utente in Filter
  */
  for(i=0; i<M; i++){
    printf("Inserisci il valore %d: ",i);
    scanf("%f",&filter[i]);
  }

  /*
  stampe di Matrix e Filter
  */
  printf("MATRICE \n");
  for(i=0; i<N; i++){

    for(j=0; j<M; j++){ 
      printf("%2d",matrix[i][j]);
    }

    printf("\n");

  }

  printf("FILTER \n");
  for(i=0; i<M; i++){
    printf("%0.2f ",filter[i]);
  }
  printf("\n");

  /*
  prodotto Matrix x Filter
  */
  for(i=0; i<N; i++){
    result[i]=0;
    for(j=0; j<M; j++){ 
      result[i] += matrix[i][j] * filter[j];
    }
  }

  /*
  stampa risultato
  */
  printf("RESULT \n");
  for(i=0; i<N; i++){
    printf("%0.2f ",result[i]);
  }


  return 0;
}