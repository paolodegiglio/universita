#include <stdio.h>

// definire il sottoprogramma; possono essere definiti altri sottoprogrammi se necessario
// Check if is prime an integer
int isPrime(int val){
	int i;
	for(i = 2; i < val/2; i++){
		if(val % i == 0){
			return 0;
		}
	}

	return 1;
}

// Get the number of num exist in the assigned number
int getCountNum(int val){
	int x = val;
	int count = 0;
	
	while(x>0) {
		x /= 10;
		count ++;
	} 

	return count;
}

// Pow function
int powCustom(int base, int exp){
	int res= 1;
	for(int i = 1; i < exp; i++){
		res *= base;
	}
	return res;
}

// Check for all the number inside the number if they are prime
int rightprime(int val){

	int returnVal = 1;

	// Check for each of the right numbers if are prime
	// For do this continue to divide the number and take the 
	// Rest of division until the number is greater than 0
	do {
		returnVal = isPrime(val);
		val = val % powCustom(10,getCountNum(val));
	} while((val > 0) && returnVal == 1);

	return returnVal;
}


int main(){
	int n, r;
	
	printf("\n Inserisci un intero positivo\n");
	do{
		scanf("%d",&n);
	}while(n<=0);

	r=rightprime(n);
	
	printf("\n Esito su %d: %d",n,r);

	/* for(int x = 1; x <= 100; x++){
		if(rightprime(x)){
			printf("\n%2d",x);
		}
	} */
	
	return 0;
}
