#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SPACER (printf("\n"))

typedef struct {
  int * * matrix;
  int row;
  int col;
}obj;

void view(obj matrixA){
  int row = matrixA.row;
  int col = matrixA.col;

  for(int i=0; i<row; i++){
    for(int j=0; j<col; j++){
      printf("%d ", matrixA.matrix[i][j]);
    } 
    printf("\n");
  }
}

obj initMatrix(int row, int col){
  int * * matrixA;

  matrixA = (int * *) malloc(row * sizeof(int *));
  for(int i=0; i<row; i++){
    matrixA[i] = (int *) malloc(col * sizeof(int));
  }

  for(int i=0; i<row; i++){
    for(int j=0; j<col; j++){
      matrixA[i][j] = rand()%10;
      printf("%d ", matrixA[i][j]);
    } 
    printf("\n");
  }
  
  obj returnObj;
  returnObj.matrix = matrixA;
  returnObj.row = row;
  returnObj.col = col;

  return returnObj;
};

obj productMatrix(obj matrixA, obj matrixB){

  int row = matrixA.row;
  int col = matrixB.col;

  int * * matrixC;

  matrixC = (int * *) malloc(row * sizeof(int *));
  for(int i=0; i<row; i++){
    matrixC[i] = (int *) malloc(col * sizeof(int));
  }
  
  for(int i=0; i<row; i++){
    for(int j=0; j<col; j++){
      int sum = 0;
      for(int x = 0; x<matrixA.col; x++)
        sum += matrixA.matrix[i][x]*matrixB.matrix[x][j];
      matrixC[i][j] = sum;
    } 
  }

  obj returnObj;
  returnObj.matrix = matrixC;
  returnObj.row = row;
  returnObj.col = col;

  return returnObj;
};

int main(){

  srand(time(NULL));
  
  obj matrixA;
  obj matrixB;
  int row = rand()%4 + 2;
  int col = rand()%4 + 2;

  printf("MATRICE A\n");
  matrixA = initMatrix(row,col); 

  SPACER;

  printf("MATRICE B\n");
  matrixB = initMatrix(col,rand()%4+2);

  obj matrixC = productMatrix(matrixA, matrixB);

  SPACER;

  int chose =0;
  do{
    SPACER;
    printf("Inserisci 1 per visualizzare la soluzione ");
    scanf("%d",&chose);
  }while(chose != 1);
  view(matrixC);

  return 1;
}
