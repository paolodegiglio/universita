#include <stdlib.h>
#include <stdio.h>

int main()
{
    int parkA = 0, parkB = 0, parkC = 0;

    // Macchina accesa o spenta

    int startCode = 0;

    do
    {
        printf("Inserisci codice di avvio: ");
        scanf("%d", &startCode);
    } while (startCode != 11111);

    // Caricamento dati dei parcheggi

    int inputPark;
    printf("Inserisci input di caricamento parcheggio: ");
    scanf("%d", &inputPark);
    if (inputPark > 31)
    {
        parkA = 31;
    }
    else
    {
        parkA = inputPark;
    }

    printf("Inserisci input di caricamento parcheggio: ");
    scanf("%d", &inputPark);
    if (inputPark > 31)
    {
        parkB = 31;
    }
    else
    {
        parkB = inputPark;
    }

    printf("Inserisci input di caricamento parcheggio: ");
    scanf("%d", &inputPark);
    if (inputPark > 24)
    {
        parkC = 24;
    }
    else
    {
        parkC = inputPark;
    }

    // Elaborazione macchina
    int in_out, sector;
    do
    {
        printf("Inserisci in_out 2bit notazione con singolo 1: ");
        scanf("%d", &in_out);
        printf("Inserisci sector 3bit notazione con singolo 1: ");
        scanf("%d", &sector);

        if (in_out == 01 || in_out == 10 || sector == 100 || sector == 010 || sector == 001)
        {
            if (sector == 100 && in_out == 10)
            {
                if (parkA < 31)
                {
                    parkA++;
                    printf("%d%d%d%d%d", 1, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
                else
                {
                    printf("%d%d%d%d%d", 0, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
            }

            if (sector == 100 && in_out == 01)
            {
                if (parkA > 0)
                {
                    parkA--;
                    printf("%d%d%d%d%d", 0, 1, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
                else
                {
                    printf("%d%d%d%d%d", 0, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
            }

            if (sector == 010 && in_out == 10)
            {
                if (parkB < 31)
                {
                    parkB++;
                    printf("%d%d%d%d%d", 1, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
                else
                {
                    printf("%d%d%d%d%d", 0, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
            }

            if (sector == 010 && in_out == 01)
            {
                if (parkB > 0)
                {
                    parkB--;
                    printf("%d%d%d%d%d", 0, 1, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
                else
                {
                    printf("%d%d%d%d%d", 0, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
            }

            if (sector == 001 && in_out == 10)
            {
                if (parkC < 24)
                {
                    parkC++;
                    printf("%d%d%d%d%d", 1, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
                else
                {
                    printf("%d%d%d%d%d", 0, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
            }

            if (sector == 001 && in_out == 01)
            {
                if (parkC > 0)
                {
                    parkC--;
                    printf("%d%d%d%d%d", 0, 1, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
                else
                {
                    printf("%d%d%d%d%d", 0, 0, (parkA >= 31) ? 1 : 0, (parkB >= 31) ? 1 : 0, (parkC >= 24) ? 1 : 0);
                }
            }

            printf("\n%.2d %.2d %.2d\n", parkA, parkB, parkC);
        }

    } while (in_out != 00 || sector != 000);

    printf("\nTERMINATO %.2d %.2d %.2d\n", parkA, parkB, parkC);

    return 0;
}